var mongoose = require("mongoose"),
    Entity = require("../../vendor/cubbyhole/Database/Entity");

function PlanException(message) {
    this.message = message;
    this.name = "PlanException";
}

module.exports = Plan = Entity.extend({
    init: function () {
        this["_super"](mongoose.model('Plan'));

        this.addPropertyRule("name", "required|alpha_num|min:5|max:30")
            .addPropertyRule("price", "required|numeric|min:0")
            .addPropertyRule("space_limit", "required|numeric|min:0")
            .addPropertyRule("download_limit", "required|numeric|min:0")
            .addPropertyRule("upload_limit", "required|numeric|min:0");
    },

    setName: function (value) {
        return this.setProperty("name", value);
    },

    setPrice: function (value) {
        return this.setProperty("price", value);
    },

    setSpaceLimit: function (value) {
        return this.setProperty("space_limit", value);
    },

    setDownloadLimit: function (value) {
        return this.setProperty("download_limit", value);
    },

    setUploadLimit: function (value) {
        return this.setProperty("upload_limit", value);
    }
});

