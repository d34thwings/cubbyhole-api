var mongoose = require("mongoose"), crypto = require("crypto"),
    Entity = require("../../vendor/cubbyhole/Database/Entity");

module.exports = ShortLink = Entity.extend({
    init: function () {
        this["_super"](mongoose.model('ShortLink'));

        this.addPropertyRule("_file", "required");
        this.addPropertyRule("link", "required|alpha_num|unique|min:1");
    },

    getFile: function() {
        return this.getProperty("_file");
    },

    setFile: function (value) {
        return this.setProperty("_file", value);
    },

    setLink: function (value) {
        return this.setProperty("link", value);
    }
});

