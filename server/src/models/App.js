var mongoose = require("mongoose"), crypto = require("crypto"),
    Entity = require("../../vendor/cubbyhole/Database/Entity");

function AppException(message) {
    this.message = message;
    this.name = "AppException";
}

module.exports = App = Entity.extend({
    init: function () {
        this["_super"](mongoose.model('OAuthApps'));

        // App name
        var nameRegexp = "^[a-zA-Z0-9-_\s]{3,30}$";
        this.addPropertyRule("name", "required|regexp:" + nameRegexp + "|min:3|max:30");

        this.addPropertyRule("clientId", "required");
        this.addPropertyRule("clientSecret", "required|alpha_num|min:32|max:32");
        this.addPropertyRule("redirectUri", "required");
        this.addPropertyRule("_client", "required");
    },

    /**
     * Sets the app's name.
     *
     * @param {String} value
     * @return {*|void}
     */
    setName: function (value) {
        return this.setProperty("name", value);
    },

    /**
     * Sets the redirect URI for the app.
     *
     * @param {String} value
     * @return {*|void}
     */
    setRedirectURI: function (value) {
        return this.setProperty("redirectUri", value);
    },

    /**
     * Sets the owner of the app.
     *
     * @param value
     * @return {*|void}
     */
    setClient: function (value) {
        return this.setProperty("_client", value);
    },

    /**
     * Generate a secret for the app.
     *
     * @param {String} clientId
     * @return {String}
     */
    generateSecret: function (clientId) {
        var timestamp = new Date().getTime();
        return crypto.createHash('md5').update(timestamp + "-" + clientId + "-cubbyhole").digest("hex") + "";
    }
});

