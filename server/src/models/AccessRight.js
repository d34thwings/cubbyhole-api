var mongoose = require("mongoose"), crypto = require("crypto"),
    Entity = require("../../vendor/cubbyhole/Database/Entity");

module.exports = AccessRight = Entity.extend({
    init: function () {
        this["_super"](mongoose.model('AccessRight'));

        this.addPropertyRule("_file", "required");
        this.addPropertyRule("_user", "required");
        this.addPropertyRule("access", "required|regexp:^[rw]$|min:0");
    },

    setFile: function (value) {
        return this.setProperty("_file", value);
    },

    setUser: function (value) {
        return this.setProperty("_user", value);
    },

    setAccess: function (value) {
        return this.setProperty("access", value);
    }
});

