var mongoose = require("mongoose"),
    Entity = require("../../vendor/cubbyhole/Database/Entity");

function ClientException(message) {
    this.message = message;
    this.name = "ClientException";
}

module.exports = Client = Entity.extend({
    init: function () {
        this["_super"](mongoose.model('Client'));

        // Client name
        var nameRegexp = "^[a-zA-Z0-9-_\s]{3,30}$";
        this.addPropertyRule("name", "required|regexp:" + nameRegexp + "|min:3|max:30");

        // Email
        var emailRegExp = "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$";
        this.addPropertyRule("email", "required|regexp:" + emailRegExp + "|max:100|unique");

        // Password
        this.addPropertyRule("password", "required|min:6|max:30")
            .excludeProperty("password")
            .setEncryptedProperty("password");
    },

    /**
     * Set the client's name.
     *
     * @param {String} value
     * @return {Client}
     */
    setName: function (value) {
        return this.setProperty("name", value);
    },

    /**
     * Set the client's email.
     *
     * @param {String} value
     * @return {Client}
     */
    setEmail: function (value) {
        return this.setProperty("email", value);
    },

    /**
     * Sets the client's password.
     *
     * @param {String} value
     * @return {Client}
     */
    setPassword: function (value) {
        return this.setProperty("password", value);
    }
});

