var mongoose = require("mongoose"),
    Entity = require("../../vendor/cubbyhole/Database/Entity");

module.exports = UploadedFile = Entity.extend({
    init: function () {
        this["_super"](mongoose.model('UploadedFile'));

        this.addPropertyRule("filename", "required|min:1|max:50")
            .addPropertyRule("_owner", "required")
            .addPropertyRule("size", "required|numeric|min:1")
            .setPropertyNullable("_folder");
    },

    getFilename: function() {
        return this.getProperty("filename");
    },

    getFileId: function() {
        return this.getProperty("_id");
    },

    /**
     * Sets the unique identifier of the file.
     *
     * @param value
     * @return {UploadedFile}
     */
    setFileId: function(value) {
        return this.setProperty("_id", value);
    },

    /**
     * Set the uploadedFile's filename.
     *
     * @param {String} value
     * @return {UploadedFile}
     */
    setFileName: function (value) {
        return this.setProperty("filename", value);
    },

    /**
     * Set the uploadedFile's email.
     *
     * @param {String} value
     * @return {UploadedFile}
     */
    setOwner: function (value) {
        return this.setProperty("_owner", value);
    },

    setFolder: function (value) {
        return this.setProperty("_folder", value);
    },

    setSize: function (value) {
        return this.setProperty("size", value);
    }
});

