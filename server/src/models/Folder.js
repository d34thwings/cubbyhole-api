var mongoose = require("mongoose"),
    Entity = require("../../vendor/cubbyhole/Database/Entity");

module.exports = Folder = Entity.extend({
    init: function () {
        this["_super"](mongoose.model('Folder'));

        this.addPropertyRule("foldername", "required|min:1")
            .addPropertyRule("_owner", "required")
            .setPropertyNullable("_parent");
    },

    /**
     * Set the Folder's foldername.
     *
     * @param {String} value
     * @return {Folder}
     */
    setFolderName: function (value) {
        return this.setProperty("foldername", value);
    },

    /**
     * Set the Folder's email.
     *
     * @param {String} value
     * @return {Folder}
     */
    setOwner: function (value) {
        return this.setProperty("_owner", value);
    },

    /**
     * Set the Folder's email.
     *
     * @param {String} value
     * @return {Folder}
     */
    setParent: function (value) {
        if(value != "") {
            return this.setProperty("_parent", value);    
        }
        else {
            return this.setProperty("_parent", null);
        }
    }
});

