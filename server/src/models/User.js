var mongoose = require("mongoose"),
    Entity = require("../../vendor/cubbyhole/Database/Entity"),
    Plan = require("../models/Plan"),
    UploadFile = require("../models/UploadedFile");

function UserException(message) {
    this.message = message;
    this.name = "UserException";
}

module.exports = User = Entity.extend({
    init: function () {
        this["_super"](mongoose.model('User'));

        this.getUsedSpace = function (callback) {
            if (typeof this.getProperty("_id") == "undefined") {
                throw new UserException("User not set. Missing find or save ?");
            }

            var userId = this.getProperty("_id");
            var planId = this.getProperty("_plan");
            new UploadFile().all({ _owner: userId }, function (err, files) {
                var plan = new Plan().findById(planId, function (err) {
                    if (err) {
                        if (typeof callback === "function")
                            callback(err);
                        return;
                    }

                    var used_space = 0;
                    files.forEach(function (file) {
                        used_space += file.size;
                    });

                    if (typeof callback === "function")
                        callback(null,  used_space, plan.getProperty("space_limit"));
                });
            });
        };

        // Email
        var emailRegExp = "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$";
        this.addPropertyRule("email", "required|regexp:" + emailRegExp + "|max:100|unique");

        // Password
        this.addPropertyRule("password", "required|min:6|max:30")
            .excludeProperty("password")
            .setEncryptedProperty("password");

        this.addPropertyRule("firstname", "required|min:2|max:30")
            .addPropertyRule("lastname", "required|min:2|max:30");
    },

    /**
     * Set the user's email.
     *
     * @return {String}
     */
    getEmail: function () {
        return this.getProperty("email");
    },

    /**
     * Sets the user's password.
     *
     * @return {String}
     */
    getPassword: function () {
        return this.getProperty("password");
    },

    /**
     * Set the user's email.
     *
     * @param {String} value
     * @return {User}
     */
    setEmail: function (value) {
        return this.setProperty("email", value);
    },

    /**
     * Sets the user's password.
     *
     * @param {String} value
     * @return {User}
     */
    setPassword: function (value) {
        return this.setProperty("password", value);
    },

    setFirstname: function (value) {
        return this.setProperty("firstname", value);
    },

    setLastname: function (value) {
        return this.setProperty("lastname", value);
    }
});

