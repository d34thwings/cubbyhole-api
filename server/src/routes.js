var Class = require("./../vendor/cubbyhole/Class"),
    RouteBuilder = require("../vendor/cubbyhole/Routing/RouteBuilder"),
    UsersController = require('./controllers/UsersController'),
    ClientsController = require('./controllers/ClientsController'),
    AppsController = require('./controllers/AppsController'),
    UploadedFilesController = require('./controllers/UploadedFilesController'),
    AccessRightsController = require('./controllers/AccessRightsController'),
    FoldersController = require('./controllers/FoldersController'),
    HomeController = require('./controllers/HomeController'),
    PlansController = require('./controllers/PlansController'),
    Response = require("../vendor/cubbyhole/Http/Response");

var rb = new RouteBuilder();

/*
 |--------------------------------------------------------------------------
 | Application Routes
 |--------------------------------------------------------------------------
 |
 | Here is where you can register all of the routes for an application.
 | It's a breeze. Simply tell Node the URIs it should respond to
 | and give it the Closure to execute when that URI is requested.
 |
 */

rb.controller("/", new HomeController());
rb.get("/f/{short_link}", new HomeController().fileByShortLink);
rb.get("/download/{file_id}", new HomeController().fileById);
rb.get("/stats", new UsersController().retrieveStats);
rb.resource("/users", new UsersController(), "user_id");
rb.resource("/plans", new PlansController(), "plan_id");
rb.resource("/clients", new ClientsController(), "client_id");
rb.resource("/apps", new AppsController(), "app_id");
rb.resource("/files", new UploadedFilesController(), "file_id");
rb.resource("/folders", new FoldersController(), "folder_id");
rb.resource("/rights", new AccessRightsController(), "access_right_id");

/*
 |--------------------------------------------------------------------------
 */

module.exports = rb.getRoutes();