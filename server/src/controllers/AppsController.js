var ResourceController = require("../../vendor/cubbyhole/Controllers/ResourceController"),
    App = require("../models/App"), Client = require("../models/Client");

module.exports = AppsController = ResourceController.extend({
    init: function () {

    },

    /**
     * Retrieve all apps.
     *
     * @param {Request} request
     * @param {Response} response
     */
    getIndex: function (request, response) {
        new App().all(request.allInputs(), function (err, apps) {
            if (err) {
                response.set(400, err, null).write();
            } else {
                response.set(200, null, apps).write();
            }
        });
    },

    /**
     * App retrieve.
     *
     * @param {Request} request
     * @param {Response} response
     */
    get: function (request, response) {
        var app = new App().findById(request.getURLComponent("app_id"), function (err) {
            if (err)
                response.set(400, err, null).write();
            else
                response.set(200, null, app.getProperties()).write();
        });
    },

    /**
     * App creation.
     *
     * @param {Request} request
     * @param {Response} response
     */
    post: function (request, response) {
        new Client().findById(request.getInput("client_id"), function (err) {
            if (err) {
                response.set(400, err, null).write();
                return;
            }

            var app = new App();
            console.log(app.generateSecret(request.getInput("client_id")));
            app.setMultipleProperties({
                name: request.getInput("name"),
                clientId: new Date().getTime() * 6,
                clientSecret: app.generateSecret(request.getInput("client_id")),
                redirectUri: request.getInput("redirect_uri"),
                _client: request.getInput("client_id")
            })
                .save(function (err) {
                    if (err)
                        response.set(400, err, null).write();
                    else
                        response.set(201, null, app.getProperties()).write();
                });
        });
    },

    /**
     * App delete.
     *
     * @param {Request} request
     * @param {Response} response
     */
    remove: function (request, response) {
        var app = new App().findById(request.getURLComponent("app_id"), function (err) {
            if (err) {
                // App not found handle
                response.set(400, err, null).write();
            } else {
                // App found
                app.destroy(function (err2) {
                    if (err2) {
                        // App delete error handle
                        response.set(400, err2, null).write();
                    } else {
                        // App delete success
                        response.set(200, null, "App has been deleted.").write();
                    }
                });
            }
        });
    },

    /**
     * App edit.
     *
     * @param {Request} request
     * @param {Response} response
     */
    put: function (request, response) {

        function putApp() {
            var app = new App().findById(request.getURLComponent("app_id"), function (err) {
                if (err) {
                    // App not found error handle
                    response.set(400, err, null).write();
                } else {
                    // App found
                    var name = request.getInput("name");
                    var redirectURI = request.getInput("redirect_uri");
                    var client = request.getInput("client_id");

                    // Set app's properties
                    if (!(typeof name === "undefined"))
                        app.setName(name);
                    if (!(typeof redirectURI === "undefined"))
                        app.setRedirectURI(redirectURI);
                    if (!(typeof client === "undefined"))
                        app.setClient(client);

                    app.save(function (err2) {
                        if (err2) {
                            // Save error handle
                            response.set(400, err2, null).write();
                        } else {
                            // Save success
                            response.set(201, null, app.getProperties()).write();
                        }
                    });
                }
            });
        }

        if (request.getInput("client_id")) {
            new Client().findById(request.getInput("client_id"), function (err) {
                if (err) {
                    response.set(400, err, null).write();
                    return;
                }

                putApp();
            });
        } else {
            putApp();
        }
    }
});