var ResourceController = require("../../vendor/cubbyhole/Controllers/ResourceController"),
    AccessRight = require("../models/AccessRight"), UploadedFile = require("../models/UploadedFile"),
    User = require("../models/User");

module.exports = AccessRightsController = ResourceController.extend({
    init: function () {

    },

    /**
     * Retrieve all accessRights.
     *
     * @param {Request} request
     * @param {Response} response
     */
    getIndex: function (request, response) {
        new AccessRight().all(request.allInputs(), function (err, accessRights) {
            if (err) {
                response.set(400, err, null).write();
            } else {
                response.set(200, null, accessRights).write();
            }
        });
    },

    /**
     * AccessRight retrieve.
     *
     * @param {Request} request
     * @param {Response} response
     */
    get: function (request, response) {
        var accessRight = new AccessRight().findById(request.getURLComponent("access_right_id"), function (err) {
            if (err)
                response.set(400, err, null).write();
            else
                response.set(200, null, accessRight.getProperties()).write();
        });
    },

    /**
     * AccessRight creation.
     *
     * @param {Request} request
     * @param {Response} response
     */
    post: function (request, response) {
        new AccessRight().find({
            _file: request.getInput("_file"),
            _user: request.getInput("_user")
        }, function (err) {
            if (!err) {
                response.set(400, [
                    {
                        code: "ALREADY_EXIST",
                        message: "That access right already exist."
                    }
                ], null).write();
                return;
            }

            var file = new UploadedFile().findById(request.getInput("_file"), function (err) {
                if (err) {
                    response.set(400, err, null).write();
                    return;
                }

                var user = new User().findById(request.getInput("_user"), function (err) {
                    if (err) {
                        response.set(400, err, null).write();
                        return;
                    }

                    if (file.getProperty('_owner').toString() == user.getProperty('_id').toString()) {
                        response.set(400, [
                            {
                                code: "INVALID_ACTION",
                                message: "You can't create an access right for the owner of the file."
                            }
                        ], null).write();
                        return;
                    }

                    var accessRight = new AccessRight();
                    accessRight.setMultipleProperties({
                        _file: request.getInput("_file"),
                        _user: request.getInput("_user"),
                        access: request.getInput("access")
                    }).save(function (err) {
                        if (err)
                            response.set(400, err, null).write();
                        else
                            response.set(201, null, accessRight.getProperties()).write();
                    });
                });
            });
        });
    },

    /**
     * AccessRight delete.
     *
     * @param {Request} request
     * @param {Response} response
     */
    remove: function (request, response) {
        var accessRight = new AccessRight().findById(request.getURLComponent("access_right_id"), function (err) {
            if (err) {
                // AccessRight not found handle
                response.set(400, err, null).write();
            } else {
                // AccessRight found
                accessRight.destroy(function (err2) {
                    if (err2) {
                        // AccessRight delete error handle
                        response.set(400, err2, null).write();
                    } else {
                        // AccessRight delete success
                        response.set(200, null, "AccessRight has been deleted.").write();
                    }
                });
            }
        });
    },

    /**
     * AccessRight edit.
     *
     * @param {Request} request
     * @param {Response} response
     */
    put: function (request, response) {
        var accessRight = new AccessRight().findById(request.getURLComponent("access_right_id"), function (err) {
            if (err) {
                // AccessRight not found error handle
                response.set(400, err, null).write();
            } else {
                // AccessRight found
                var access = request.getInput("access");

                // Set accessRight's properties
                if (!(typeof access === "undefined"))
                    accessRight.setAccess(access);

                accessRight.save(function (err2) {
                    if (err2) {
                        // Save error handle
                        response.set(400, err2, null).write();
                    } else {
                        // Save success
                        response.set(201, null, accessRight.getProperties()).write();
                    }
                });
            }
        });
    }
});