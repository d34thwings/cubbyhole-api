var ResourceController = require("../../vendor/cubbyhole/Controllers/ResourceController"),
    UploadedFile = require("../models/UploadedFile"),
    User = require("../models/User"),
    Folder = require("../models/Folder");

module.exports = UploadedFilesController = ResourceController.extend({
    init: function () {

    },

    /**
     * Retrieve all uploadedFiles.
     *
     * @param {Request} request
     * @param {Response} response
     */
    getIndex: function (request, response) {
        new UploadedFile().all(request.allInputs(), function (err, uploadedFiles) {
            if (err) {
                response.set(400, err, null).write();
            } else {
                response.set(200, null, uploadedFiles).write();
            }
        });
    },

    /**
     * UploadedFile retrieve.
     *
     * @param {Request} request
     * @param {Response} response
     */
    get: function (request, response) {
        var uploadedFile = new UploadedFile().findById(request.getURLComponent("file_id"), function (err) {
            if (err)
                response.set(400, err, null).write();
            else
                response.set(200, null, uploadedFile.getProperties()).write();
        });
    },

    /**
     * UploadedFile creation.
     *
     * @param {Request} request
     * @param {Response} response
     */
    post: function (request, response) {
        response.set(400, [
            {
                code: 400,
                error: "bad_request",
                error_description: "File upload isn't allowed through POST, use sockets instead."
            }
        ], null).write();
    },

    /**
     * UploadedFile delete.
     *
     * @param {Request} request
     * @param {Response} response
     */
    remove: function (request, response) {
        var mongoose = require("mongoose");
        var uploadedFile = new UploadedFile().findById(request.getURLComponent("file_id"), function (err) {
            if (err) {
                // UploadedFile not found handle
                response.set(400, err, null).write();
            } else {
                // UploadedFile found
                request.getServer().gfs.remove({ _id: uploadedFile.getId() }, function (err) {
                    if (err) throw err;
                    uploadedFile.destroy(function (err) {
                        if (err) {
                            // UploadedFile delete error handle
                            response.set(400, err, null).write();
                        } else {
                            // UploadedFile delete success
                            response.set(200, null, "UploadedFile has been deleted.").write();
                        }
                    });
                });
            }
        });
    },

    /**
     * UploadedFile edit.
     *
     * @param {Request} request
     * @param {Response} response
     */
    put: function (request, response) {
        var uploadedFile = new UploadedFile().findById(request.getURLComponent("file_id"), function (err) {
            if (err) {
                // UploadedFile not found error handle
                response.set(400, err, null).write();
            } else {
                // UploadedFile found
                var filename = request.getInput("filename");
                var folder = request.getInput("_folder");

                // Set uploadedFile's properties
                if (!(typeof filename === "undefined"))
                    uploadedFile.setFileName(filename);

                new Folder().findById(folder, function (err2) {
                    if (!err2) {
                        uploadedFile.setFolder(folder);
                    } else {
                        if (!(typeof folder === "undefined") && folder != "" && folder != null) {
                            response.set(400, err2, null).write();
                            return;
                        } else {
                            uploadedFile.setFolder(null);
                        }
                    }

                    uploadedFile.save(function (err3) {
                        if (err3) {
                            // Save error handle
                            response.set(400, err3, null).write();
                        } else {
                            // Save success
                            response.set(201, null, uploadedFile.getProperties()).write();
                        }
                    });
                });
            }
        });
    }
});