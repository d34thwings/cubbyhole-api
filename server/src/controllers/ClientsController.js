var ResourceController = require("../../vendor/cubbyhole/Controllers/ResourceController"),
    Client = require("../models/Client");

module.exports = ClientsController = ResourceController.extend({
    init: function () {

    },

    /**
     * Retrieve all clients.
     *
     * @param {Request} request
     * @param {Response} response
     */
    getIndex: function (request, response) {
        new Client().all(request.allInputs(), function (err, clients) {
            if (err) {
                response.set(400, err, null).write();
            } else {
                response.set(200, null, clients).write();
            }
        });
    },

    /**
     * Client retrieve.
     *
     * @param {Request} request
     * @param {Response} response
     */
    get: function (request, response) {
        var client = new Client().findById(request.getURLComponent("client_id"), function (err) {
            if (err)
                response.set(400, err, null).write();
            else
                response.set(200, null, client.getProperties()).write();
        });
    },

    /**
     * Client creation.
     *
     * @param {Request} request
     * @param {Response} response
     */
    post: function (request, response) {
        var client = new Client()
            .setName(request.getInput("name"))
            .setEmail(request.getInput("email"))
            .setPassword(request.getInput("password"))
            .save(function (err) {
                if (err)
                    response.set(400, err, null).write();
                else
                    response.set(201, null, client.getProperties()).write();
            });
    },

    /**
     * Client delete.
     *
     * @param {Request} request
     * @param {Response} response
     */
    remove: function (request, response) {
        var client = new Client().findById(request.getURLComponent("client_id"), function (err) {
            if (err) {
                // Client not found handle
                response.set(400, err, null).write();
            } else {
                // Client found
                client.destroy(function (err2) {
                    if (err2) {
                        // Client delete error handle
                        response.set(400, err2, null).write();
                    } else {
                        // Client delete success
                        response.set(200, null, "Client has been deleted.").write();
                    }
                });
            }
        });
    },

    /**
     * Client edit.
     *
     * @param {Request} request
     * @param {Response} response
     */
    put: function (request, response) {
        var client = new Client().findById(request.getURLComponent("client_id"), function (err) {
            if (err) {
                // Client not found error handle
                response.set(400, err, null).write();
            } else {
                // Client found
                var name = request.getInput("name");
                var email = request.getInput("email");
                var password = request.getInput("password");

                // Set client's properties
                if (!(typeof name === "undefined"))
                    client.setName(name);
                if (!(typeof email === "undefined"))
                    client.setEmail(email);
                if (!(typeof password === "undefined"))
                    client.setPassword(password);

                client.save(function (err2) {
                    if (err2) {
                        // Save error handle
                        response.set(400, err2, null).write();
                    } else {
                        // Save success
                        response.set(201, null, client.getProperties()).write();
                    }
                });
            }
        });
    }
});