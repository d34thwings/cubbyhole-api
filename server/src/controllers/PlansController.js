var ResourceController = require("../../vendor/cubbyhole/Controllers/ResourceController"),
    Plan = require("../models/Plan");

module.exports = PlansController = ResourceController.extend({
    init: function () {

    },

    /**
     * Retrieve all plans.
     *
     * @param {Request} request
     * @param {Response} response
     */
    getIndex: function (request, response) {
        new Plan().all(request.allInputs(), function (err, plans) {
            if (err) {
                response.set(400, err, null).write();
            } else {
                response.set(200, null, plans).write();
            }
        });
    },

    /**
     * Plan retrieve.
     *
     * @param {Request} request
     * @param {Response} response
     */
    get: function (request, response) {
        var plan = new Plan().findById(request.getURLComponent("plan_id"), function (err) {
            if (err)
                response.set(400, err, null).write();
            else
                response.set(200, null, plan.getProperties()).write();
        });
    },

    /**
     * Plan creation.
     *
     * @param {Request} request
     * @param {Response} response
     */
    post: function (request, response) {
        var plan = new Plan()
            .setName(request.getInput("name"))
            .setPrice(parseInt(request.getInput("price")))
            .setSpaceLimit(parseInt(request.getInput("space_limit")))
            .setDownloadLimit(parseInt(request.getInput("download_limit")))
            .setUploadLimit(parseInt(request.getInput("upload_limit")))
            .save(function (err) {
                if (err)
                    response.set(400, err, null).write();
                else
                    response.set(201, null, plan.getProperties()).write();
            });
    },

    /**
     * Plan delete.
     *
     * @param {Request} request
     * @param {Response} response
     */
    remove: function (request, response) {
        var plan = new Plan().findById(request.getURLComponent("plan_id"), function (err) {
            if (err) {
                // Plan not found handle
                response.set(400, err, null).write();
            } else {
                // Plan found
                plan.destroy(function (err2) {
                    if (err2) {
                        // Plan delete error handle
                        response.set(400, err2, null).write();
                    } else {
                        // Plan delete success
                        response.set(200, null, "Plan has been deleted.").write();
                    }
                });
            }
        });
    },

    /**
     * Plan edit.
     *
     * @param {Request} request
     * @param {Response} response
     */
    put: function (request, response) {
        var plan = new Plan().findById(request.getURLComponent("plan_id"), function (err) {
            if (err) {
                // Plan not found error handle
                response.set(400, err, null).write();
            } else {
                // Plan found
                var name = request.getInput("name");
                var price = request.getInput("price");
                var spaceLimit = request.getInput("space_limit");
                var downloadLimit = request.getInput("download_limit");
                var uploadLimit = request.getInput("upload_limit");

                // Set plan's properties
                if (!(typeof name === "undefined"))
                    plan.setName(name);
                if (!(typeof price === "undefined"))
                    plan.setPrice(price);
                if (!(typeof spaceLimit === "undefined"))
                    plan.setSpaceLimit(spaceLimit);
                if (!(typeof downloadLimit === "undefined"))
                    plan.setDownloadLimit(downloadLimit);
                if (!(typeof uploadLimit === "undefined"))
                    plan.setUploadLimit(uploadLimit);

                plan.save(function (err2) {
                    if (err2) {
                        // Save error handle
                        response.set(400, err2, null).write();
                    } else {
                        // Save success
                        response.set(201, null, plan.getProperties()).write();
                    }
                });
            }
        });
    }
});