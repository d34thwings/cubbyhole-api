var Controller = require("../../vendor/cubbyhole/Controllers/Controller"),
    ShortLink = require("../models/ShortLink"), UploadedFile = require("../models/UploadedFile"),
    User = require("../models/User"), Folder = require("../models/Folder"),
    AccessRight = require("../models/AccessRight");

module.exports = AccessRightsController = Controller.extend({
    init: function () {
        this.removeAuthFilter(this.fileByShortLink);
    },

    getIndex: function (request, response) {
        response.set(200, null, {hello: "world"}).write();
    },

    /**
     * Creates a short link to a file.
     *
     * @param response
     * @param request
     */
    postShortlink: function (request, response) {
        new UploadedFile().findById(request.getInput("file_id"), function (err) {
            if (err) {
                response.set(400, err, null).write();
                return;
            }

            var shortLink = new ShortLink().find({ _file: request.getInput("file_id") }, function (err) {
                if (err) {
                    shortLink.count(function (err, count) {
                        shortLink.setFile(request.getInput("file_id"))
                            .setLink(count.toString(16))
                            .save(function (err2) {
                                if (err2) {
                                    response.set(400, err2, null).write();
                                    return;
                                }
                                response.set(200, null, shortLink.getProperties()).write();
                            });
                    });

                    return;
                }

                response.set(200, null, shortLink.getProperties()).write();
            });
        });
    },

    /**
     * Retrieves account hierarchy.
     *
     * @param {Request} response
     * @param {Response} request
     */
    getHierarchy: function (request, response) {
        var user = request.getUser();
        new Folder().all({ _owner: user.id }, function (err, folders) {
            if (err) {
                response.set(400, err, null).write();
                return;
            }

            new UploadedFile().all({ _owner: user.id }, function (err, files) {
                if (err) {
                    response.set(400, err, null).write();
                    return;
                }

                function buildPath(parent) {
                    parent.folders = [];
                    folders.forEach(function (folder) {
                        if (parent._id == folder._parent) {
                            parent.folders.push(buildPath(folder));
                        }
                    });
                    parent.files = [];
                    files.forEach(function (file) {
                        if (parent._id == file._folder) {
                            parent.files.push(file);
                        }
                    });
                    return parent;
                }

                response.set(200, null, buildPath({})).write();
            });
        });
    },

    /**
     * Download a file by its id (access restrictions are applied).
     *
     * @param {Request} request
     * @param {Response} response
     */
    fileById: function (request, response) {
        var uploadedFile = new UploadedFile().findById(request.getURLComponent("file_id"), function (err) {
            if (err) {
                response.set(400, err, null).write();
                return;
            }

            function sendFile() {
                response.setHeader('Content-disposition', 'attachment; filename=' + uploadedFile.getFilename());

                var readStream = request.getServer().gfs.createReadStream({
                    _id: uploadedFile.getFileId()
                });

                readStream.on('error', function (err) {
                    response.set(400, err, null).write();
                });

                readStream.pipe(response.getHttpResponse())
            }

            var user = request.getUser();
            if (user.id != uploadedFile.getProperty('_owner').toString()) {
                new AccessRight().find({
                    _owner: user.id,
                    _file: request.getURLComponent("file_id")
                }, function (err) {
                    if (err) {
                        response.set(400, err, null).write();
                        return;
                    }

                    sendFile();
                });
                return;
            }

            sendFile();
        });
    },

    /**
     * Download a file by short link.
     *
     * @param {Request} response
     * @param {Response} request
     */
    fileByShortLink: function (request, response) {
        var shortLink = new ShortLink().find({ link: request.getURLComponent("short_link") }, function (err) {
            if (err) {
                response.set(400, err, null).write();
                return;
            }

            var uploadedFile = new UploadedFile().findById(shortLink.getFile(), function (err) {
                if (err) {
                    response.set(400, err, null).write();
                    return;
                }

                response.setHeader('Content-disposition', 'attachment; filename=' + uploadedFile.getFilename());

                var readStream = request.getServer().gfs.createReadStream({
                    _id: uploadedFile.getFileId()
                });

                readStream.on('error', function (err) {
                    response.set(400, err, null).write();
                });

                readStream.pipe(response.getHttpResponse())
            });
        });
    }
});