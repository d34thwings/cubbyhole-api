var ResourceController = require("../../vendor/cubbyhole/Controllers/ResourceController"),
	Folder = require("../models/Folder"),
	User = require("../models/User"),
    UploadedFile = require("../models/UploadedFile");

module.exports = FoldersController = ResourceController.extend({
	init: function () {

	},

	/**
	 * Retrieve all folders.
	 *
	 * @param {Request} request
	 * @param {Response} response
	 */
	getIndex: function (request, response) {
		new Folder().all(request.allInputs(), function (err, folders) {
			if (err) {
				response.set(400, err, null).write();
			} else {
				response.set(200, null, folders).write();
			}
		});
	},

	/**
	 * folder retrieve.
	 *
	 * @param {Request} request
	 * @param {Response} response
	 */
	get: function (request, response) {
		var folder = new Folder().findById(request.getURLComponent("folder_id"), function (err) {
            if (err) {
                return response.set(400, err, null).write();
            }

            if (request.getInput("show") != true) {
                return response.set(200, null, folder.getProperties()).write();
            }

            var parentFolder = folder.getProperties();
            new UploadedFile().all({
                _folder: request.getURLComponent("folder_id")
            }, function (err, files) {
                if (err) {
                    return response.set(400, err, null).write();
                }

                parentFolder["files"] = files;

                new Folder().all({
                    _parent: request.getURLComponent("folder_id")
                }, function (err, folders) {
                    if (err) {
                        return {};
                    }

                    parentFolder["folders"] = folders;
                    response.set(200, null, parentFolder).write();
                });
            });
		});
	},

	/**
	 * Folder creation.
	 *
	 * @param {Request} request
	 * @param {Response} response
	 */
	post: function (request, response) {
		var folder = new Folder().setFolderName(request.getInput("foldername")).setParent(request.getInput("_parent")).setOwner(request.getInput("_owner")).save(function (err) {
			if (err)
				response.set(400, err, null).write();
			else
				response.set(201, null, folder.getProperties()).write();
		});
	},

	/**
	 * Folder delete.
	 *
	 * @param {Request} request
	 * @param {Response} response
	 */
	remove: function (request, response) {
		var mongoose = require("mongoose");
		var folder = new Folder().findById(request.getURLComponent("folder_id"), function (err) {
			if (err) {
				// Folder not found handle
				response.set(400, err, null).write();
			} else {
				// Folder found
				request.getServer().gfs.remove({ _id: folder.getId() }, function (err) {
					if (err) throw err;
					folder.destroy(function (err) {
						if (err) {
							// Folder delete error handle
							response.set(400, err, null).write();
						} else {
							// Folder delete success
							response.set(200, null, "Folder has been deleted.").write();
						}
					});
				});
			}
		});
	},

	/**
	 * UploadedFile edit.
	 *
	 * @param {Request} request
	 * @param {Response} response
	 */
	put: function (request, response) {
		var folder = new Folder().findById(request.getURLComponent("folder_id"), function (err) {
			if (err) {
				// UploadedFile not found error handle
				response.set(400, err, null).write();
			} else {
				// UploadedFile found
				var foldername = request.getInput("foldername");
				var owner = request.getInput("_owner");
				var parent = request.getInput("_parent");
				var path = request.getInput("path");

				// Set uploadedFile's properties
				if (!(typeof foldername === "undefined")){
					folder.setFolderName(foldername);
				}

                new Folder().findById(parent, function (err2) {
                    if (!err2) {
                        folder.setParent(parent);
                    } else {
                        if (!(typeof parent === "undefined") && parent != "" && parent != null) {
                            response.set(400, err2, null).write();
                            return;
                        } else {
                            folder.setParent(null);
                        }
                    }

                    folder.save(function (err3) {
                        if (err3) {
                            // Save error handle
                            response.set(400, err3, null).write();
                        } else {
                            // Save success
                            response.set(201, null, folder.getProperties()).write();
                        }
                    });
                });
			}
		});
	},

    fetchFolder: function (parent) {
        new Folder().all({
            _parent: parent
        }, function (err, folders) {
            if (err) {
                return {};
            }

            return folders;
        });
    },

    fetchFiles: function (parent) {
        new UploadedFile().all({
            _folder: parent
        }, function (err, files) {
            if (err) {
                return {};
            }

            new Folder().all({
                _parent: parent
            }, function (err, folders) {
                if (err) {
                    return {};
                }

                return folders;
            });
        });
    }
});