var ResourceController = require("../../vendor/cubbyhole/Controllers/ResourceController"),
    User = require("../models/User"), UploadedFile = require("../models/UploadedFile"),
    Plan = require("../models/Plan");

module.exports = UsersController = ResourceController.extend({
    init: function () {
        this.removeAuthFilter(this.post);
    },

    /**
     * Retrieve all users.
     *
     * @param {Request} request
     * @param {Response} response
     */
    getIndex: function (request, response) {
        new User().all(request.allInputs(), function (err, users) {
            if (err) {
                response.set(400, err, null).write();
            } else {
                response.set(200, null, users).write();
            }
        });
    },

    /**
     * User retrieve.
     *
     * @param {Request} request
     * @param {Response} response
     */
    get: function (request, response) {
        var user = new User().findById(request.getURLComponent("user_id"), function (err) {
            if (err)
                response.set(400, err, null).write();
            else
                response.set(200, null, user.getProperties()).write();
        });
    },

    /**
     * User creation.
     *
     * @param {Request} request
     * @param {Response} response
     */
    post: function (request, response) {
        var user = new User()
            .setEmail(request.getInput("email"))
            .setPassword(request.getInput("password"))
            .setFirstname(request.getInput("firstname"))
            .setLastname(request.getInput("lastname"))
            .save(function (err) {
                if (err)
                    response.set(400, err, null).write();
                else
                    response.set(201, null, user.getProperties()).write();
            });
    },

    /**
     * User delete.
     *
     * @param {Request} request
     * @param {Response} response
     */
    remove: function (request, response) {
        var user = new User().findById(request.getURLComponent("user_id"), function (err) {
            if (err) {
                // User not found handle
                response.set(400, err, null).write();
            } else {
                // User found
                user.destroy(function (err2) {
                    if (err2) {
                        // User delete error handle
                        response.set(400, err2, null).write();
                    } else {
                        // User delete success
                        response.set(200, null, "User has been deleted.").write();
                    }
                });
            }
        });
    },

    /**
     * User edit.
     *
     * @param {Request} request
     * @param {Response} response
     */
    put: function (request, response) {
        var user = new User().findById(request.getURLComponent("user_id"), function (err) {
            if (err) {
                // User not found error handle
                response.set(400, err, null).write();
            } else {
                // User found
                var email = request.getInput("email");
                var password = request.getInput("password");

                // Set email and/or passwords if they were given
                if (!(typeof email === "undefined"))
                    user.setEmail(email);
                if (!(typeof password === "undefined"))
                    user.setPassword(password);
                user.save(function (err2) {
                    if (err2) {
                        // Save error handle
                        response.set(400, err2, null).write();
                    } else {
                        // Save success
                        response.set(201, null, user.getProperties()).write();
                    }
                });
            }
        });
    },

    /**
     * Retrieves all the user stats.
     *
     * @param {Request} request
     * @param {Response} response
     */
    retrieveStats: function (request, response) {
        var user = new User().findById(request.getUser().id, function (err) {
            if (err) {
                response.set(400, err, null).write();
                return;
            }

            user.getUsedSpace(function (err, used_space, storage_limit) {
                var stats = {
                    used_space: used_space,
                    storage_size: storage_limit,
                    free_space: storage_limit - used_space
                };

                response.set(200, null, stats).write();
            });
        });
    }
});