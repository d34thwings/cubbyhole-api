var fs = require('fs'), mongoose = require('mongoose');

function main(config) {
    var Server = require("../vendor/cubbyhole/Networking/Server");

    var routes = require('./routes');

    // Config for deployment
    config.server.port = process.env.OPENSHIFT_NODEJS_PORT ? process.env.OPENSHIFT_NODEJS_PORT : config.server.port;
    config.server.bind_ip = process.env.OPENSHIFT_NODEJS_IP ? process.env.OPENSHIFT_NODEJS_IP : config.server.bind_ip;

    var server = new Server(config, routes);

    server.onDatabaseConnected(function () {
        require("./schemas/UserSchema");
        require("./schemas/ClientSchema");
        require("./schemas/PlanSchema");
        require("./schemas/FileSchema");
        require("./schemas/ShortLinkSchema");
        require("./schemas/AccessRightSchema");
        require("./schemas/FolderSchema");
    });

    var User = require('./models/User');
    server.getUserLogin(function (req, res, callback) {
        var user = new User();
        user.find({
            email: req.body.email,
            password: req.body.password
        }, function (err) {
            callback(err, user);
        });
    });

    var UploadedFile = require('./models/UploadedFile');
    var Folder = require('./models/Folder');

    server.onFileUpload(function (socket, stream, data) {
        var id = mongoose.Types.ObjectId();
        if (data._folder == "")
            data._folder = null;

        new Folder().findById(data._folder, function (err) {
            if (err != null && data._folder != null && data._folder != "") {
                server.socketSendError(socket, 400, "invalid_request", err);
                return;
            }

            var file = null;

            var user = new User().findById(data.access_token.userId, function (err) {
                if (err) {
                    server.socketSendError(socket, 400, "invalid_request", err);
                    return;
                }

                user.getUsedSpace(function (err, used_space, storage_limit) {
                    if (err) {
                        server.socketSendError(socket, 400, "invalid_request", err);
                        return;
                    }

                    // Remove existing file if exist
                    file = new UploadedFile().find({
                        _owner: data.access_token.userId,
                        _folder: data._folder,
                        filename: data.filename
                    }, function (err) {
                        if (!err && used_space - file.getProperty("size") + data.size > storage_limit) {
                            server.socketSendError(socket, 400, "not_enough_space", null);
                            return;
                        }

                        if (err && used_space + data.size > storage_limit) {
                            server.socketSendError(socket, 400, "not_enough_space", null);
                            return;
                        }

                        var writeStream = server.gfs.createWriteStream({
                            _id: id,
                            filename: data.filename
                        });
                        stream.pipe(writeStream);

                        var received = 0;
                        stream.on('data', function (chunk) {
                            received += chunk.length;
                        });

                        socket.on('disconnect', function () {
                            if (data.size == received) return;
                            deleteFile(id);
                        });

                        function deleteFile(fileId, callback) {
                            server.gfs.remove({ _id: fileId }, function () {
                                if (file) {
                                    file.destroy(callback);
                                }
                            });
                        }

                        writeStream.on('close', function (f) {
                            if (data.size != received) {
                                writeStream.destroy();
                                file = null;
                                deleteFile(id);
                                return;
                            }

                            function saveFile() {
                                // Save new file
                                file = new UploadedFile()
                                    .setFileName(data.filename)
                                    .setOwner(data.access_token.userId)
                                    .setFileId(id)
                                    .setFolder(data._folder)
                                    .setSize(data.size)
                                    .save(function (err2) {
                                        if (err2) {
                                            deleteFile(id);
                                            server.socketSendError(socket, 400, "invalid_request", err2);
                                            return;
                                        }

                                        socket.emit("upload_success", file.getProperties());
                                    });
                            }

                            if (!err) {
                                deleteFile(file.getProperty('_id'), function () {
                                    saveFile();
                                });
                            }

                            saveFile();
                        });
                    });
                });
            });
        });
    });

    server.listen();
}

function getConfigFile(path, callback) {
    fs.readFile(path, 'utf8', function (err, json_string) {
        if (err) {
            console.error("Could not open config file:", err.path);
            callback(null);
        } else {
            callback(JSON.parse(json_string));
        }
    });
}

var customConfigPath = './server/src/config/config.json';
getConfigFile(customConfigPath, function (localConfig) {
    if (localConfig) {
        main(localConfig);
    } else {
        main(null);
    }
});