var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var shortLinkSchema = Schema({
    _file: { type: Schema.Types.ObjectId, ref: 'UploadedFile' },
    link: { type: String, match: /^[a-zA-Z0-9]+$/ }
});

var ShortLinkModel = mongoose.model('ShortLink', shortLinkSchema);
exports.model = ShortLinkModel;
exports.schema = shortLinkSchema;