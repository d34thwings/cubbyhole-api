var mongoose = require('mongoose'), Schema = mongoose.Schema;

var planSchema = Schema({
    name: { type: String },
    price: { type: Number },
    space_limit: { type: Number },
    download_limit: { type: Number },
    upload_limit: { type: Number }
});

var PlanModel = mongoose.model('Plan', planSchema);
exports.model = PlanModel;
exports.schema = planSchema;