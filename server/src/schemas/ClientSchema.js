var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var clientSchema = Schema({
    name: { type: String, match: /^[a-zA-Z0-9-_\s]{3,30}$/ },
    email: { type: String, match: /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/ },
    password: { type: String, match: /^[a-zA-Z0-9]+$/ },
    date: { type: Date, default: Date.now },
    apps: [
        { type: Schema.Types.ObjectId, ref: 'OAuthApps' }
    ]
});

var ClientModel = mongoose.model('Client', clientSchema);
exports.model = ClientModel;
exports.schema = clientSchema;