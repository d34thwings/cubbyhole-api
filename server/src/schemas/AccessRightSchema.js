var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var accessRightSchema = Schema({
    _file: { type: Schema.Types.ObjectId, ref: 'UploadedFile' },
    _user: { type: Schema.Types.ObjectId, ref: 'User' },
    access: { type: String, match: /^[rw]$/ }
});

var AccessRightModel = mongoose.model('AccessRight', accessRightSchema);
exports.model = AccessRightModel;
exports.schema = accessRightSchema;