var mongoose = require('mongoose'), Schema = mongoose.Schema;

var userSchema = Schema({
    firstname: { type: String },
    lastname: { type: String },
    email: { type: String, match: /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/ },
    password: { type: String, match: /^[a-zA-Z0-9]+$/ },
    date: { type: Date, default: Date.now },
    _plan: { type: Schema.Types.ObjectId, ref: 'Plan' }
});

var UserModel = mongoose.model('User', userSchema);
exports.model = UserModel;
exports.schema = userSchema;