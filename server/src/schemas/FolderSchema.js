var mongoose = require('mongoose'), Schema = mongoose.Schema;

var folderSchema = Schema({
    foldername: { type: String },
    date: { type: Date, default: Date.now },
    _parent: { type: Schema.Types.ObjectId, ref: 'Folder' },
    _owner: { type: Schema.Types.ObjectId, ref: 'User' },
    path: { type: String }
});

var FolderModel = mongoose.model('Folder', folderSchema);
exports.model = FolderModel;
exports.schema = folderSchema;