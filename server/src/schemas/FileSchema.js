var mongoose = require('mongoose'), Schema = mongoose.Schema;

var fileSchema = Schema({
    filename: { type: String },
    date: { type: Date, default: Date.now },
    _owner: { type: Schema.Types.ObjectId, ref: 'User' },
    _folder: { type: Schema.Types.ObjectId, ref: 'Folder' },
    size: { type: Number }
});

var FileModel = mongoose.model('UploadedFile', fileSchema);
exports.model = FileModel;
exports.schema = fileSchema;