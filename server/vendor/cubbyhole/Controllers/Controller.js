var _ = require("underscore"),
    Class = require("../Class");

module.exports = Controller = Class.extend({
    init: function () {

    },

    /**
     * Return all methods that should be routed by the server.
     *
     * @return {Array}
     */
    getRoutes: function () {
        var httpVerbs = ["get", "post", "put", "delete"];
        var routes = [];
        _.each(this.__proto__, function (method, methodName) {
            if (methodName == "init" || methodName == "getIndex") return;
            var name = methodName.toLowerCase();

            httpVerbs.forEach(function (verb) {
                if (name.indexOf(verb) == 0 && name.substring(verb.length) != "") {
                    routes.push({ type: verb.toUpperCase(), path: name.substring(verb.length), methodName: methodName });
                }
            });
        });
        return routes;
    },

    /**
     * Function called on the index route of the controller.
     */
    getIndex: function () {
        throw "Method getIndex not overridden";
    },

    removeAuthFilter: function (method) {
        method.noauth = true;
    }
});