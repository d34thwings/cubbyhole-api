var Controller = require("./Controller");

module.exports = ResourceController = Controller.extend({
    init: function () {

    },

    list: function () {
        throw "LIST method not overridden";
    },

    get: function () {
        throw "GET method not overridden";
    },

    post: function () {
        throw "POST method not overridden";
    },

    remove: function () {
        throw "DELETE method not overridden";
    },

    put: function () {
        throw "PUT method not overridden";
    }
});