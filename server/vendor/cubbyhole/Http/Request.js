var url = require("url"), _ = require("underscore"),
    Class = require("../Class");

module.exports = Request = Class.extend({
    init: function (httpRequest, postData) {
        var url_parts = url.parse(httpRequest.url, true);
        var query = url_parts.query;
        var _auth = null;

        /**
         * Contains the components of the routed path.
         * Example : if the route is /my_route/{my_component}
         * and the path is /my_route/123, the value of
         * URL Components will be { my_component: 123 }
         *
         * @type {Object}
         */
        var URLComponents = {};

        /**
         * Object reflection.
         *
         * @type {Request}
         */
        var self = this;

        /**
         * Tells if the request has been routed yet.
         *
         * @type {Boolean}
         */
        var routed = false;

        this.setAuth = function (middleware) {
            _auth = middleware;
            return self;
        };

        /**
         * Path of the request.
         *
         * @returns {String}
         */
        this.path = function () {
            return url_parts.pathname + "";
        };

        /**
         * Get the request type (GET, POST, PUT, DELETE).
         *
         * @returns {String}
         */
        this.type = function () {
            return httpRequest.method;
        };

        /**
         * Returns all inputs given either by GET or POST.
         * If some data keys are duplicated in GET and POST data, POST data
         * will override GET data.
         *
         * @returns {Object}
         */
        this.allInputs = function () {
            var inputs = query;
            if (this.type() == "POST" || this.type() == "PUT") {
                _.each(postData, function (value, key) {
                    inputs[key] = value;
                });
            }
            delete inputs._method;
            return inputs;
        };

        /**
         * Returns the value of specific input. Input can be of type GET or POST.
         *
         * @param key
         * @returns {*}
         */
        this.getInput = function (key) {
            return this.allInputs()[key];
        };

        /**
         * Retrieves the value of a route URL component.
         * Like {id} for exemple.
         *
         * @param {String} key
         * @return {String}
         */
        this.getURLComponent = function (key) {
            return URLComponents[key];
        };

        /**
         * Returns the api server object.
         *
         * @return {Server}
         */
        this.getServer = function () {
            return httpRequest.server;
        };

        /**
         * Returns the current logged user.
         *
         * @return {User}
         */
        this.getUser = function () {
            return httpRequest.user;
        };

        /**
         * Search through the given routes for a route that suites
         * the request path and call it's callback.
         *
         * @param {RoutesContainer} routesContainer
         * @param {Response} response
         */
        this.dispatch = function (routesContainer, response) {
            var routes = null;
            switch (this.type()) {
                case "GET":
                    routes = routesContainer.GETroutes;
                    break;
                case "POST":
                    routes = routesContainer.POSTroutes;
                    break;
                case "PUT":
                    routes = routesContainer.PUTroutes;
                    break;
                case "DELETE":
                    routes = routesContainer.DELETEroutes;
                    break;
            }

            // Search for a matching route
            _.each(routes, function (route) {
                if (routed)  return;

                // Replace url components by regexp for matching
                var routePath = route.path.replace(/\{[A-z0-9]+\}/, "[a-z0-9]+");
                var regexp = new RegExp("^" + routePath + "$", 'i');

                // Check matching between given path and route regexp
                if (self.path().match(regexp)) {
                    // Retrieve URL components values
                    URLComponents = {};
                    var routeComponents = route.path.split("/");
                    var pathComponents = self.path().split("/");

                    // Store components with their value
                    _.each(routeComponents, function (component, i) {
                        if (component.match(/^\{[A-z0-9]+\}$/)) {
                            var key = component.replace(/[\{\}]/g, "");
                            URLComponents[key] = pathComponents[i];
                        }
                    });

                    // Mark request as routed and call the route callback
                    routed = true;
                    if (route.callback.noauth) {
                        route.callback(self, response);
                    } else {
                        _auth(httpRequest, response.get(), function (err) {
                            if (err) {
                                response.set(err.code, [{ code: err.error, message: err.error_description }], null).write();
                                return;
                            }
                            route.callback(self, response);
                        });
                    }
                }
            });

            // If no route was found, send 404 not found
            if (!routed) {
                response.set(404, "Given path isn't handled", null).write();
            }
        };

        /**
         * Returns if the request has been routed.
         *
         * @returns {Boolean}
         */
        this.isRouted = function () {
            return routed;
        }
    }
});