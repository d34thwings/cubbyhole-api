var Class = require("../Class");

module.exports = Response = Class.extend({
    init: function (httpResponse) {
        /**
         * Http status for the response.
         *
         * @type {Number}
         **/
        this.status = null;

        /**
         * Error(s) to transmit into the response.
         *
         * @type {String|Array}
         */
        this.errors = null;

        /**
         * Object to transmit into the response.
         *
         * @type {Object}
         */
        this.response = null;

        /**
         * Tells if the response has been written.
         *
         * @private
         * @type {boolean}
         */
        var responseDone = false;

        /**
         * return the original response object.
         *
         * @returns {*}
         */
        this.getHttpResponse = function() {
            return httpResponse;
        };

        /**
         * Store the given http status code, errors and response.
         *
         * @param {Number} status
         * @param {String| Array} errors
         * @param {Object} response
         * @returns {Response}
         */
        this.set = function (status, errors, response) {
            this.status = status;
            this.errors = errors;
            this.response = response;

            return this;
        };

        /**
         * Set header for the response.
         *
         * @param {String} header
         * @param {*} value
         * @returns {Response}
         */
        this.setHeader = function(header, value) {
            httpResponse.setHeader(header, value);
            return this;
        };

        /**
         * Write the content of the response and end request.
         *
         * @return {Response}
         */
        this.write = function () {
            // Can't write the response is already marked as done
            if (this.done()) return this;

            httpResponse.writeHead(this.status, {"Content-Type": "application/json"});
            httpResponse.write(this.toJSON());
            httpResponse.end();
            responseDone = true;
            return this;
        };

        /**
         * Get a JSON string of the response.
         *
         * @returns {String}
         */
        this.toJSON = function () {
            return JSON.stringify({ status: this.status, errors: this.errors, response: this.response });
        };

        /**
         * Return if the response has already been made or not.
         *
         * @returns {Boolean}
         */
        this.done = function () {
            return responseDone;
        };

        this.get = function () {
            return httpResponse;
        };
    }
});