var _ = require("underscore"), crypto = require("crypto"),
    Class = require("../Class");

function EntityException(message) {
    this.message = message;
    this.name = "EntityException";
}

module.exports = Entity = Class.extend({
    /**
     * Creates an entity linked to the given database schema.
     *
     * @param {Object} EntityModel
     */
    init: function (EntityModel) {
        /**
         * Reflection object.
         *
         * @private
         * @type {Entity}
         */
        var self = this;

        /**
         * Contains all the properties and their values of the entity.
         *
         * @type {Object}
         * @private
         */
        var _properties = {};

        /**
         * Fields in this array will not be retrieved by getProperties.
         *
         * @private
         */
        var _excluded = [];

        /**
         * Adds a property to the excluded property array.
         *
         * @param {String} property
         * @return {Entity}
         */
        this.excludeProperty = function (property) {
            if (!(typeof property === "string") || property == "") throw new EntityException("Invalid property");
            if (typeof _properties[property] === "undefined") throw new EntityException("Property not found");
            if (_excluded.indexOf(property) == -1)
                _excluded.push(property);
            return this;
        };

        /**
         * Fields in this array can be saved with a null value.
         *
         * @private
         */
        var _nullable = [];

        /**
         * Adds a property to the list of nullable properties.
         *
         * @param {String} property
         * @return {Entity}
         */
        this.setPropertyNullable = function (property) {
            if (!(typeof property === "string") || property == "") throw new EntityException("Invalid property");
            if (typeof _properties[property] === "undefined") throw new EntityException("Property not found");
            if (_nullable.indexOf(property) == -1)
                _nullable.push(property);
            return this;
        };

        // Generate and store properties from the EntityModel.
        _.each(EntityModel.schema.paths, function (path, prop) {
            if (prop == "__v") return;
            _properties[prop] = null;
        });

        /**
         * Returns the value of the given property.
         *
         * @param {String} property
         * @return {*}
         */
        this.getProperty = function (property) {
            if (!this.propertyExists(property)) throw new EntityException("Invalid property");
            return _properties[property];
        };

        /**
         * Returns all the properties of the entity.
         *
         * @return {Object}
         */
        this.getProperties = function () {
            var props = _properties;
            _excluded.forEach(function (prop) {
                delete props[prop];
            });
            if (_model)
                props._id = _model._id;
            return props;
        };

        /**
         * Get the id in the database of the entity.
         * Returns a valid id only after a find or a save.
         *
         * @return {String}
         */
        this.getId = function () {
            return _model ? _model._id : null;
        };

        /**
         * Contains all rules for property setters.
         *
         * @type {Object}
         * @private
         */
        var _rules = {};

        /**
         * Add a rule for a property of the entity. This rule will be checked each time
         * you want to edit a field of the entity.
         *
         * Rule example : required|alpha_num|unique
         *
         * Valid rules :
         * required, numeric, alpha_num, regexp:{string}, max:{number}, min:{number}, unique
         *
         * @param {String} property
         * @param {String} rule
         * @return {Entity}
         */
        this.addPropertyRule = function (property, rule) {
            if (!this.propertyExists(property)) throw new EntityException("Invalid property");
            _rules[property] = rule;
            return this;
        };

        /**
         * Contains all the properties name that should be unique into database.
         *
         * @private
         */
        var _uniqueProperties = [];

        /**
         * Contains all the properties that should be encrypted on save.
         *
         * @type {Array}
         * @private
         */
        var _encryptedProperties = [];

        /**
         * Add a property to the encrypted properties array.
         *
         * @param property
         */
        this.setEncryptedProperty = function (property) {
            if (_encryptedProperties.indexOf(property) != -1) return this;
            if (!this.propertyExists(property)) throw new EntityException("Invalid property");
            _encryptedProperties[_encryptedProperties.length] = property;
            return this;
        };

        /**
         * Retrieves a match from the database and convert it to the given model.
         * /!\Doesn't checks the compatibility so be careful using this./!\
         *
         * @param {String} property
         * @param {Entity} model
         * @param {Function} callback
         */
        this.getForeignKey = function (property, model, callback) {
            if (!this.propertyExists(property)) throw new EntityException("Invalid property.");
            if (typeof EntityModel.schema.paths[property].options.ref === "undefined") throw new EntityException("Property is not a foreign key.");
            var fk = new model().findById(_properties[property], function (err) {
                callback.call(self, err, fk);
            });
        };

        var _defaultValues = [];

        this.setDefaultValue = function (property, value) {
            if (typeof value === "object")
                throw new EntityException("Default value can't be an object.");
            if (!this.propertyExists(property)) throw new EntityException("Invalid property " + property);

        };

        /**
         * Contains all the properties that have been edited.
         *
         * @type {Array}
         * @private
         */
        var _editedProperties = [];

        /**
         * Set the property to the specified value.
         *
         * @param {String} property
         * @param {*} value
         */
        this.setProperty = function (property, value) {
            _properties[property] = value;
            if (_editedProperties.indexOf(property) == -1)
                _editedProperties[_editedProperties.length] = property;
            return this;
        };

        /**
         * Assign multiple properties at once.
         *
         * @param {Object} keyValues
         * @return {Entity}
         */
        this.setMultipleProperties = function (keyValues) {
            _.each(keyValues, function (value, key) {
                self.setProperty(key, value);
            });
            return this;
        };

        /**
         * Checks if a property exists.
         *
         * @param {String} property
         * @return {boolean}
         */
        this.propertyExists = function (property) {
            return !(!(typeof property === "string") || property == "" || typeof _properties[property] === "undefined");
        };

        /**
         * Checks if the value passes the property rules.
         *
         * @param {String} property
         * @param {*} value
         * @throws {EntityException}
         */
        this.checkProperty = function (property, value) {
            if (!this.propertyExists(property)) throw new EntityException("Invalid property");

            // Check rules
            if (!(typeof _rules[property] === "undefined")) {
                _rules[property].split("|").forEach(function (component) {
                    switch (component) {
                        case "required":
                            if (value == null || (typeof value === "string" && value == ""))
                                throw new EntityException("Property " + property + " is required, so it cannot be null or empty.");
                            break;
                        case "numeric":
                            if (!(typeof value === "number"))
                                throw new EntityException("Property " + property + " must be a numeric value.");
                            break;
                        case "alpha_num":
                            if (!(typeof value === "string") && !(typeof value === "number"))
                                throw new EntityException("Property " + property + " must be a number or a string.");
                            if (typeof value === "string" && !value.match(/^[A-z0-9]*$/))
                                throw new EntityException("Property " + property + " must contain only alphanumeric characters.");
                            break;
                        case "unique":
                            if (_uniqueProperties.indexOf(property) == -1)
                                _uniqueProperties.push(property);
                            break;
                    }
                    if (component.indexOf("regexp:") == 0) {
                        var regexp = new RegExp(component.split(":")[1]);
                        if (!(typeof value === "string"))
                            throw new EntityException("Property " + property + " must be a string.");
                        if (!value.match(regexp))
                            throw new EntityException("Property " + property + " doesn't match given pattern.");
                    }
                    if (component.indexOf("min:") == 0) {
                        if (typeof value === "string" && value.length < component.split(":")[1]) {
                            throw new EntityException("Property " + property + " must contain at least " + component.split(":")[1] + " characters.");
                        } else if (typeof value === "number" && value < component.split(":")[1]) {
                            throw new EntityException("Property " + property + " must be greater or equal than " + component.split(":")[1] + ".");
                        }
                    }
                    if (component.indexOf("max:") == 0) {
                        if (typeof value === "string" && value.length > component.split(":")[1]) {
                            throw new EntityException("Property " + property + " must not contain more than " + component.split(":")[1] + " characters.");
                        } else if (typeof value === "number" && value > component.split(":")[1]) {
                            throw new EntityException("Property " + property + " must be lesser or equal than " + component.split(":")[1] + ".");
                        }
                    }
                });
            }
        };

        var _model = null;

        /**
         * Save this entity to the database.
         *
         * @param {Function} [callback]
         */
        this.save = function (callback) {
            // Check all properties
            var errors = [];
            _.each(_properties, function (value, key) {
                if (!(typeof _rules[key] === "undefined") && _editedProperties.indexOf(key) != -1) {
                    try {
                        self.checkProperty(key, value);
                    } catch (e) {
                        errors.push({code: "INVALID_FIELD", message: e.message, field: key, rule: _rules[key]});
                    }
                }
            });

            if (errors.length > 0) {
                if (typeof callback === "function")
                    callback.call(this, errors);
                return this;
            }

            // Encryption
            _encryptedProperties.forEach(function (property) {
                if (_editedProperties.indexOf(property) != -1)
                    _properties[property] = crypto.createHash('md5').update(_properties[property]).digest("hex");
            });

            // Build unique keys array
            var queries = [];
            _uniqueProperties.forEach(function (property) {
                var query = {};
                query[property] = _properties[property];
                queries.push(query);
            });

            if (_model == null) {
                // New
                EntityModel.findOne({}).or(queries).exec(function (err, entity) {
                    if (entity) {
                        var str = "";
                        _uniqueProperties.forEach(function (property) {
                            str += property + ", "
                        });
                        if (typeof callback === "function")
                            callback.call(self, [
                                { code: "NOT_UNIQUE_FIELD", message: "Properties " + str + "must be unique values." }
                            ]);
                    } else {
                        var props = {};
                        _.each(_properties, function (value, prop) {
                            if ((_properties[prop] || _nullable.indexOf(prop) != -1)) props[prop] = _properties[prop];
                        });
                        EntityModel.create(props, function (err, entity) {
                            _editedProperties = [];
                            if (!err) {
                                _model = entity;
                                _.each(_properties, function (value, prop) {
                                    _properties[prop] = _model[prop];
                                });
                            }
                            if (typeof callback === "function")
                                callback.call(self, err ? [
                                    { code: "MONGOOSE_ERROR", message: err }
                                ] : null);
                        });
                    }
                });
            } else {
                // Update
                EntityModel.findOne({}).or(queries).exec(function (err, entity) {
                    if (entity && entity._id.toString() != _model._id.toString()) {
                        var str = "";
                        _uniqueProperties.forEach(function (property) {
                            str += property + ", "
                        });
                        if (typeof callback === "function")
                            callback.call(self, [
                                { code: "NOT_UNIQUE_FIELD", message: "Properties " + str + "must be unique values." }
                            ]);
                    } else {
                        var props = {};
                        _.each(_properties, function (value, prop) {
                            if ((_properties[prop] || _nullable.indexOf(prop) != -1) && prop != "_id") props[prop] = _properties[prop];
                        });
                        EntityModel.update({ _id: _model._id }, props, function (err) {
                            _editedProperties = [];
                            if (typeof callback === "function")
                                callback.call(self, err ? [
                                    { code: "MONGOOSE_ERROR", message: err }
                                ] : null);
                        });
                    }
                });
            }

            return this;
        };

        /**
         * Retrieves an entity from the database by its id.
         *
         * @param {String|ObjectId} id
         * @param {Function} [callback]
         * @return {Entity}
         */
        this.findById = function (id, callback) {
            EntityModel.findById(id, function (err, entity) {
                if (entity) {
                    _model = entity;
                    _.each(_properties, function (value, prop) {
                        _properties[prop] = _model[prop];
                    });
                    if (typeof callback === "function")
                        callback.call(self, err ? [
                            { code: "MONGOOSE_ERROR", message: err }
                        ] : null);
                } else {
                    if (typeof callback === "function")
                        callback.call(self, [
                            { code: "ENTITY_NOT_FOUND", message: "Find " + EntityModel.modelName + " with id '" + id + "' failed." }
                        ]);
                }
            });

            return this;
        };

        /**
         * Retrieves one match from the database with the given conditions.
         *
         * @param {Object} conditions
         * @param {Function} [callback]
         * @return {Entity}
         */
        this.find = function (conditions, callback) {
            // Check conditions
            _.each(conditions, function (value, property) {
                if (!(typeof property === "string") || property == "") throw new EntityException("Invalid property");
                if (typeof _properties[property] === "undefined") {
                    console.error("Property ", property, "not found");
                    delete conditions[property];
                }

                if (_encryptedProperties.indexOf(property) != -1)
                    conditions[property] = crypto.createHash('md5').update(value).digest("hex");
            });

            EntityModel.findOne(conditions).exec(function (err, entity) {
                if (entity) {
                    _model = entity;
                    _.each(_properties, function (value, prop) {
                        _properties[prop] = _model[prop];
                    });
                    if (typeof callback === "function")
                        callback.call(self, err ? [
                            { code: "MONGOOSE_ERROR", message: err }
                        ] : null);
                } else {
                    if (typeof callback === "function")
                        callback.call(self, [
                            { code: "ENTITY_NOT_FOUND", message: "Find " + EntityModel.modelName + " with given conditions failed." }
                        ]);
                }
            });

            return this;
        };

        /**
         * Removes this entity from database.
         *
         * @param {Function} callback
         * @return {Entity}
         */
        this.destroy = function (callback) {
            if (_model == null) {
                throw new EntityException("Entity isn't linked to database. Find or save missing ?");
            } else {
                EntityModel.remove({ _id: _model._id }, function (err) {
                    _model = null;
                    if (typeof callback === "function")
                        callback.call(self, err ? [
                            { code: "MONGOOSE_ERROR", message: err }
                        ] : null);
                });
            }

            return this;
        };

        /**
         * Gets all entities of this type.
         *
         * @param {Object} conditions
         * @param {Function} callback
         * @return {Entity}
         */
        this.all = function (conditions, callback) {
            if (!(typeof  conditions === "object")) throw new EntityException("Invalid argument. Conditions must be an object.");

            // Check conditions
            _.each(conditions, function (value, property) {
                if (!(typeof property === "string") || property == "") throw new EntityException("Invalid property");
                if (typeof _properties[property] === "undefined") {
                    console.error("Property", property, "not found");
                    delete conditions[property];
                }

                if (_encryptedProperties.indexOf(property) != -1)
                    conditions[property] = crypto.createHash('md5').update(value).digest("hex");
            });

            EntityModel.find(conditions).exec(function (err, results) {
                if (!err) {
                    var entities = [];
                    _.each(results, function (entity, i) {
                        entities[i] = {};
                        _.each(_properties, function (value, prop) {
                            if (_excluded.indexOf(prop) == -1)
                                entities[i][prop] = entity[prop];
                        });
                        entities[i]._id = entity._id.toString();
                    });
                }
                if (typeof callback === "function")
                    callback.call(self, err ? [
                        { code: "MONGOOSE_ERROR", message: err }
                    ] : null, entities);
            });
            return this;
        };

        /**
         * Counts the number of elements using this model.
         *
         * @param {Function} callback
         * @return {Entity}
         */
        this.count = function(callback) {
            EntityModel.count().exec(callback);
            return this;
        };
    }
});