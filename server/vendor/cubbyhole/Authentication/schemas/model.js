var mongoose = require('mongoose'), crypto = require("crypto"),
    Schema = mongoose.Schema,
    model = module.exports;

/*
 |--------------------------------------------------------------------------
 | Schemas
 |--------------------------------------------------------------------------
 |
 | Below are all the schemas required for the OAuth 2.0 authentication
 | for the following grants : password, refresh token and authorization code.
 |
 */

var OAuthAccessTokensSchema = new Schema({
    accessToken: { type: String },
    clientId: { type: String, ref: 'OAuthApps' },
    userId: { type: String, ref: 'User' },
    expires: { type: Date }
});

var OAuthRefreshTokensSchema = new Schema({
    refreshToken: { type: String },
    clientId: { type: String, ref: 'OAuthApps' },
    userId: { type: String, ref: 'User' },
    expires: { type: Date }
});

var OAuthAppsSchema = new Schema({
    name: { type: String },
    clientId: { type: String },
    clientSecret: { type: String },
    redirectUri: { type: String },
    _client: { type: String, ref: 'Client' }
});

var OAuthAuthorizationCodesSchema = new Schema({
    authCode: { type: String },
    clientId: { type: String },
    expires: { type: Date },
    userId: { type: String }
});

mongoose.model('OAuthAccessTokens', OAuthAccessTokensSchema);
mongoose.model('OAuthRefreshTokens', OAuthRefreshTokensSchema);
mongoose.model('OAuthApps', OAuthAppsSchema);
mongoose.model('OAuthAuthorizationCodes', OAuthAuthorizationCodesSchema);

var OAuthAccessTokensModel = mongoose.model('OAuthAccessTokens'),
    OAuthRefreshTokensModel = mongoose.model('OAuthRefreshTokens'),
    OAuthAuthorizationCodesModel = mongoose.model('OAuthAuthorizationCodes'),
    OAuthAppsModel = mongoose.model('OAuthApps'),
    OAuthUsersModel = mongoose.model('User');

/*
 |--------------------------------------------------------------------------
 | Node OAuth 2.0 callbacks
 |--------------------------------------------------------------------------
 |
 | Below are all the methods require by node-oauth2-server.
 |
 */

/**
 * Retrieves an access token from the database.
 *
 * @param {String} bearerToken
 * @param {Function} callback
 */
model.getAccessToken = function (bearerToken, callback) {
    OAuthAccessTokensModel.findOne({ accessToken: bearerToken }, callback);
};

/**
 * Retrieves an application from the database.
 *
 * @param {String} clientId
 * @param {String} clientSecret
 * @param {Function} callback
 */
model.getClient = function (clientId, clientSecret, callback) {
    var query = { clientId: clientId };
    if (clientSecret)
        query.clientSecret = clientSecret;
    OAuthAppsModel.findOne(query, callback);
};

/**
 * Array that contains the strict list of apps allowed to use password grant.
 *
 * @type {string[]}
 */
var authorizedClientIds = ['8392379448660'];

/**
 * Checks wherever if an app has access to the given grant type.
 *
 * @param {String} clientId
 * @param {String} grantType
 * @param {Function} callback
 * @return {*}
 */
model.grantTypeAllowed = function (clientId, grantType, callback) {
    if (grantType === 'password') {
        return callback(false, authorizedClientIds.indexOf(clientId) >= 0);
    }

    callback(false, true);
};

/**
 * Save an access token into the database.
 *
 * @param {String} token
 * @param {String} clientId
 * @param {Date} expires
 * @param {Object} user
 * @param {Function} callback
 */
model.saveAccessToken = function (token, clientId, expires, user, callback) {
    var accessToken = new OAuthAccessTokensModel({
        accessToken: token,
        clientId: clientId,
        userId: user.id,
        expires: expires
    });

    accessToken.save(callback);
};

/*
 |--------------------------------------------------------------------------
 | Password grant
 |--------------------------------------------------------------------------
 |
 | Methods required for the password grant.
 |
 */

/**
 * Retrieves a user from the database.
 *
 * @param {String} email
 * @param {String} password
 * @param {Function} callback
 */
model.getUser = function (email, password, callback) {
    OAuthUsersModel.findOne({ email: email, password: crypto.createHash('md5').update(password).digest("hex") }, callback);
};

/*
 |--------------------------------------------------------------------------
 | Refresh token grant
 |--------------------------------------------------------------------------
 |
 | Methods required for the refresh token grant.
 |
 */

/**
 * Save the refresh token to the database.
 *
 * @param {String} token
 * @param {String} clientId
 * @param {Date} expires
 * @param {Object} user
 * @param {Function} callback
 */
model.saveRefreshToken = function (token, clientId, expires, user, callback) {
    var refreshToken = new OAuthRefreshTokensModel({
        refreshToken: token,
        clientId: clientId,
        userId: user.id,
        expires: expires
    });

    refreshToken.save(callback);
};

/**
 * Get a refresh token from the database.
 *
 * @param {String} refreshToken
 * @param {Function} callback
 */
model.getRefreshToken = function (refreshToken, callback) {
    OAuthRefreshTokensModel.findOne({ refreshToken: refreshToken }, callback);
};

/*
 |--------------------------------------------------------------------------
 | Authorization code grant
 |--------------------------------------------------------------------------
 |
 | Methods required for the authorization code grant.
 |
 */

/**
 * Retrieves an authorization code from the database.
 *
 * @param {String} authCode
 * @param {Function} callback
 */
model.getAuthCode = function (authCode, callback) {
    OAuthAuthorizationCodesModel.findOne({ authCode: authCode }, callback);
};

/**
 * Save an authorization code to the database.
 *
 * @param {String} code
 * @param {String} clientId
 * @param {Date} expires
 * @param {String} user
 * @param {Function} callback
 */
model.saveAuthCode = function (code, clientId, expires, user, callback) {
    var authCode = new OAuthAuthorizationCodesModel({
        authCode: code,
        clientId: clientId,
        expires: expires,
        userId: user.id
    });

    authCode.save(callback);
};