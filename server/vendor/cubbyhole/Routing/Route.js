var Class = require("../Class");

module.exports = Route = Class.extend({
    /**
     * Create a route for the given path and the given Http verb.
     *
     * @param {String} path
     * @param {String} type
     * @param {Function} callback
     */
    init: function (path, type, callback) {
        this.path = path;
        this.type = type;
        this.callback = callback;
    },

    /**
     * Converts route to String.
     *
     * @return {String}
     */
    toString: function () {
        return this.type + " " + this.path;
    }
});