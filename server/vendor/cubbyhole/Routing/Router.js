var _ = require('underscore'), url = require("url"), qs = require('querystring'),
    Class = require("../Class"),
    Response = require("../Http/Response"),
    Request = require("../Http/Request");

module.exports = Router = Class.extend({
    /**
     * Router for http server.
     *
     * @param {RoutesContainer} routes
     */
    init: function (routes) {
        this.routes = routes;
        this.auth = null;
    },

    setAuthenticationMiddleware: function (middleware) {
        if (typeof middleware === "function")
            this.auth = middleware;
    },

    /**
     * Route the given request. If no route was linked to this request an exception will be thrown.
     *
     * @param request
     * @param response
     */
    route: function (request, response) {
        var self = this;

        if (request.method == 'POST' || request.method == 'PUT') {
            new Request(request, request.body).setAuth(self.auth).dispatch(self.routes, new Response(response));
        } else {
            new Request(request).setAuth(self.auth).dispatch(self.routes, new Response(response));
        }
    }
});