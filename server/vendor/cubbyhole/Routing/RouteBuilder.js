var Class = require("../Class"),
    Route = require("./Route"),
    RoutesContainer = require("./RoutesContainer");

module.exports = RouteBuilder = Class.extend({
    init: function () {
        var routesContainer = new RoutesContainer();

        /**
         * Add a route to the RouteBuilder.
         *
         * @param route
         */
        this.addRoute = function (route) {
            switch (route.type) {
                case "GET":
                    routesContainer.GETroutes.push(route);
                    break;
                case "POST":
                    routesContainer.POSTroutes.push(route);
                    break;
                case "PUT":
                    routesContainer.PUTroutes.push(route);
                    break;
                case "DELETE":
                    routesContainer.DELETEroutes.push(route);
                    break;
            }
        };

        /**
         * Returns all routes contained in the RouteBuilder
         *
         * @return {RoutesContainer}
         */
        this.getRoutes = function () {
            return routesContainer;
        };
    },

    /**
     * Add a route for the given route and GET http verb.
     *
     * @param {string} path
     * @param {function} callback
     */
    get: function (path, callback) {
        this.addRoute(new Route(path, "GET", callback));
    },

    /**
     * Add a route for the given route and POST http verb.
     *
     * @param {string} path
     * @param {function} callback
     */
    post: function (path, callback) {
        this.addRoute(new Route(path, "POST", callback));
    },

    /**
     * Add a route for the given route and DELETE http verb.
     *
     * @param {string} path
     * @param {function} callback
     */
    remove: function (path, callback) {
        this.addRoute(new Route(path, "DELETE", callback));
    },

    /**
     * Add a route for the given route and PUT http verb.
     *
     * @param {string} path
     * @param {function} callback
     */
    put: function (path, callback) {
        this.addRoute(new Route(path, "PUT", callback));
    },

    /**
     * Creates routes to a controller.
     *
     * @param {String} path
     * @param {Object} controller
     */
    controller: function (path, controller) {
        this.get(path, controller.getIndex);

        path = path == "/" ? "" : path;

        var self = this;
        controller.getRoutes().forEach(function (route) {
            switch (route.type) {
                case "GET":
                    self.get(path + "/" + route.path, controller[route.methodName]);
                    break;
                case "POST":
                    self.post(path + "/" + route.path, controller[route.methodName]);
                    break;
                case "PUT":
                    self.put(path + "/" + route.path, controller[route.methodName]);
                    break;
                case "DELETE":
                    self.remove(path + "/" + route.path, controller[route.methodName]);
                    break;
            }
        });
    },

    /**
     * Creates routes to a resource controller.
     *
     * @param {String} path
     * @param {Object} controller
     * @param {String} [key]
     */
    resource: function (path, controller, key) {
        if (!(typeof key === "string") || key == "")
            key = "id";
        key = "{" + key + "}";

        this.controller(path, controller);
        this.get(path + "/" + key, controller.get);
        this.post(path, controller.post);
        this.put(path + "/" + key, controller.put);
        this.remove(path + "/" + key, controller.remove);
    }
});