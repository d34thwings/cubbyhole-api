var Class = require("../Class");

module.exports = RoutesContainer = Class.extend({
    init: function () {
        this.GETroutes = [];
        this.POSTroutes = [];
        this.PUTroutes = [];
        this.DELETEroutes = [];
    }
});