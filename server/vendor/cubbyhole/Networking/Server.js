var Class = require("../Class");

module.exports = Server = Class.extend({
    /**
     *
     * @param {Object} config
     * @param {RouteBuilder} routes
     * @param filters
     */
    init: function (config, routes, filters) {
        this.config = config;
        this.routes = routes;
        this.gfs = null;
    },

    listen: function () {
        var http = require("http"), socket_io = require("socket.io"), mongoose = require("mongoose"),
            fs = require('fs'), _ = require("underscore"), express = require('express'),
            oauthserver = require('node-oauth2-server'), bodyParser = require('body-parser'),
            cookieParser = require('cookie-parser'), path = require('path'), ss = require('socket.io-stream'),
            methodOverride = require('method-override'), stylus = require('stylus'), nib = require('nib'),
            Server = require('../Networking/Server'),
            Router = require('../Routing/Router'),
            Grid = require('gridfs-stream');

        /**
         * Reflection object.
         *
         * @type {Server}
         */
        var self = this;

        function getConfigFile(path, callback) {
            fs.readFile(path, 'utf8', function (err, json_string) {
                if (err) {
                    throw "Could not open default config file";
                } else {
                    callback(JSON.parse(json_string));
                }
            });
        }

        // Load default config file
        getConfigFile("./server/vendor/cubbyhole/Networking/config/default_config.json", function (defaultConfig) {

            /**
             * Redundantly checks if the given config matches all fields contained in the default config.
             *
             * @param {Object} c
             * @param {Object} d
             */
            function checkConfig(c, d) {
                if (c == null)
                    c = d;

                _.each(d, function (e, i) {
                    if (typeof c[i] === "undefined") {
                        c[i] = e;
                    } else {
                        if (typeof c[i] === "object")
                            checkConfig(c[i], e);
                    }
                });
            }

            checkConfig(self.config, defaultConfig);

            if (process.env.OPENSHIFT_NODEJS_IP) {
                self.config["server"]["bind_ip"] = process.env.OPENSHIFT_NODEJS_IP;
            }
            if (process.env.OPENSHIFT_NODEJS_PORT) {
                self.config["server"]["port"] = process.env.OPENSHIFT_NODEJS_PORT;
            }

            // Connect to database
            var mongodbURL = "mongodb://" + self.config["database"]["host"] + "/" + self.config["database"]["dbname"];
            if (process.env.OPENSHIFT_MONGODB_DB_URL) {
                mongodbURL = process.env.OPENSHIFT_MONGODB_DB_URL + self.config["database"]["dbname"];
            }
            console.log(mongodbURL);
            var dbConnection = mongoose.connect(mongodbURL).connection;
            dbConnection.once("open", function (err) {
                if (err) {
                    throw err;
                }

                console.log("Connected to database.");
                if (self.database_connect_callback)
                    self.database_connect_callback();

                // Setup GridFS
                self.gfs = Grid(dbConnection.db, mongoose.mongo);

                // Building server
                var router = new Router(self.routes);
                var app = express();

                // View engine setup
                app.set('views', 'server/src/views');
                app.set('view engine', 'jade');
                app.use(stylus.middleware({
                    src: __dirname + '/../../../public',
                    compile: function (str, path) {
                        return stylus(str).set('filename', path).set('compress', true).use(nib()).import('nib');
                    }
                }));
                app.use(express.static(__dirname + '/../../../public'));
                
                // Middleware: Allows cross-domain requests (CORS)
                var allowCrossDomain = function(req, res, next) {
                    res.header('Access-Control-Allow-Origin', '*');
                    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
                    res.header('Access-Control-Allow-Headers', 'Accept, Authorization, Content-Type, X-Requested-With');

                    if (req.method == 'OPTIONS') {
                        res.send(200);
                        return;
                    }

                    next();
                };

                // Express configuration
                app.configure(function () {
                    app.oauth = oauthserver({
                        model: require('./../Authentication/schemas/model'),
                        grants: ['password', 'refresh_token', 'authorization_code'],
                        debug: true
                    });
                    app.use(bodyParser.json());
                    app.use(bodyParser.urlencoded());
                    app.use(cookieParser('CUBBYHOLE123'));
                    app.use(express.session());
                    app.use(methodOverride());
                    app.use(allowCrossDomain);
                });

                // Create entry for web client
                var OAuthAppsModel = mongoose.model("OAuthApps");
                OAuthAppsModel.findById("535eecb4494b63f02bb51b67", function (err, webClient) {
                    if (err) throw err;
                    if (webClient) {
                        webClient.remove(function () {
                            OAuthAppsModel.create({
                                _id: "535eecb4494b63f02bb51b67",
                                _client: null,
                                redirectUri: "http://api.cubbyhole.com",
                                clientId: "8392379448660",
                                clientSecret: "9305d621f19b88c629efe85410d1eb0b",
                                name: "CubbyholeWebClient"
                            });
                        });
                        return;
                    }
                    OAuthAppsModel.create({
                        _id: "535eecb4494b63f02bb51b67",
                        _client: null,
                        redirectUri: "http://api.cubbyhole.com",
                        clientId: "8392379448660",
                        clientSecret: "9305d621f19b88c629efe85410d1eb0b",
                        name: "CubbyholeWebClient"
                    }, function (err) {
                        if (err) throw err;
                    });
                });

                // Default plan
                var PlanModel = mongoose.model("Plan");
                PlanModel.findById("535eecb4494b63f02bb51b64", function (err, free) {
                    if (err) throw err;
                    if (free) {
                        free.remove();
                    }
                    PlanModel.create({
                        _id: "535eecb4494b63f02bb51b64",
                        name: "Free trial",
                        price: 0,
                        space_limit: 5368709120,
                        download_limit: 100,
                        upload_limit: 100
                    }, function (err) {
                        if (err) throw err;
                    });
                });

                // Create entry for administrator
                var UsersModel = mongoose.model("User");
                UsersModel.findById("535eecb4494b63f02bb51b66", function (err, admin) {
                    if (err) throw err;
                    if (admin) {
                        admin.remove();
                    }
                    UsersModel.create({
                        _id: "535eecb4494b63f02bb51b66",
                        firstname: "Administrator",
                        lastname: "Cubbyhole",
                        email: "admin@cubbyhole.com",
                        password: "4d6d0500bff71423c96aa8c0e3a64655",
                        date: new Date(),
                        _plan: "535eecb4494b63f02bb51b64"
                    }, function (err) {
                        if (err) throw err;
                    });
                });

                // Access token request
                app.all('/oauth/token', app.oauth.grant());

                // Authorization page
                app.get('/oauth/authorise', function (req, res) {
                    if (!req.session.user) {
                        // If they aren't logged in, send them to your own login implementation
                        return res.redirect('/login?redirect=' + req.path + '&client_id=' +
                            req.query.client_id + '&redirect_uri=' + req.query.redirect_uri);
                    }

                    return res.render('authorise', {
                        client_id: req.query.client_id,
                        redirect_uri: req.query.redirect_uri
                    });
                });

                // Handle authorise
                app.post('/oauth/authorise', function (req, res, next) {
                    if (!req.session.user) {
                        return res.redirect('/login?client_id=' + req.query.client_id +
                            '&redirect_uri=' + req.query.redirect_uri);
                    }
                    req.body.username = req.session.user.email;
                    req.body.password = req.session.user.password;
                    next();
                }, app.oauth.authCodeGrant(function (req, next) {
                    next(null, req.body.allow === 'yes', req.session.user);
                }));

                // Show login
                app.get('/login', function (req, res) {
                    return res.render('login', {
                        redirect: req.query.redirect,
                        client_id: req.query.client_id,
                        redirect_uri: req.query.redirect_uri
                    });
                });

                // Handle login
                app.post('/login', function (req, res) {
                    if (self.login_middleware) {
                        self.login_middleware(req, res, function (err, user) {
                            if (err || !user) {
                                res.render('login', {
                                    redirect: req.body.redirect,
                                    client_id: req.body.client_id,
                                    redirect_uri: req.body.redirect_uri
                                });
                            } else {
                                req.session.user = user.getProperties();
                                return res.redirect((req.query.redirect || '/oauth/authorise') + '?client_id=' +
                                    req.query.client_id + '&redirect_uri=' + req.query.redirect_uri);
                            }
                        });
                    }
                });

                // API routes
                router.setAuthenticationMiddleware(app.oauth.authorise());
                app.all('/*' , function (req, res) {
                    req.server = self;
                    router.route(req, res);
                });

                // Error handling
                app.use(app.oauth.errorHandler());

                // Socket.io start
                var httpServer = http.createServer(app);
                var io = socket_io.listen(httpServer);

                // Start http listening
                httpServer.listen(self.config["server"]["port"], self.config["server"]["bind_ip"]);
                console.info("HTTP server is listening on " + self.config["server"]["bind_ip"] + ":" + self.config["server"]["port"] + ".");

                // Handle file uploads
                io.on('connection', function (socket) {
                    ss(socket).on('upload', function (stream, data) {
                        self.authorizeSocket(app.oauth, socket, data, function () {
                            if (self.file_upload__callback)
                                self.file_upload__callback(socket, stream, data);
                        });
                    });
                    socket.on('disconnect', function () {
                        if (self.disconnect__callback)
                            self.disconnect__callback(socket);
                    });
                });
                console.info("Socket server is listening for file uploads on /upload.");

                console.info("Server is fully started.");
            });
        });
    },

    /**
     * Function to call when server is connected to database
     *
     * @param {Function} callback
     */
    onDatabaseConnected: function (callback) {
        if (!(typeof callback === "function")) throw "Callback must be a function";
        this.database_connect_callback = callback;
    },

    /**
     * Specifies the function to use to ensure user login.
     *
     * @param {Function} middleware
     */
    getUserLogin: function (middleware) {
        if (typeof middleware === "function")
            this.login_middleware = middleware;
    },

    /**
     * Function to call wherever the server receives a file upload event.
     *
     * @param {Function} callback
     */
    onFileUpload: function (callback) {
        if (!(typeof callback === "function")) throw "Callback must be a function";
        this.file_upload__callback = callback;
    },

    /**
     * Function to call wherever a socket disconnects from the server.
     *
     * @param {Function} callback
     */
    onSocketDisconnect: function (callback) {
        if (!(typeof callback === "function")) throw "Callback must be a function";
        this.disconnect__callback = callback;
    },

    /**
     * Check for the access token in a socket event.
     *
     * @param {OAuth2Server} oauth
     * @param {Socket} socket
     * @param {Object} data
     * @param {Function} callback
     */
    authorizeSocket: function (oauth, socket, data, callback) {
        var self = this;

        oauth.model.getAccessToken(data.access_token, function (err, token) {
            if (err) return self.socketSendError(socket, 400, "server_error", err);

            if (!token) return self.socketSendError(socket, 400, "invalid_token", "The access token provided is invalid.");

            if (token.expires !== null &&
                (!token.expires || token.expires < new Date())) {
                return self.socketSendError(socket, 400, "invalid_token", "The access token provided has expired.");
            }

            data.access_token = token;
            callback();
        });
    },

    /**
     * Emit an error event to the socket.
     *
     * @param {Socket} socket
     * @param {Number} code
     * @param {String} error
     * @param {String} message
     */
    socketSendError: function (socket, code, error, message) {
        socket.emit("error", {
            code: code,
            error: error,
            error_description: message
        });
    }
});