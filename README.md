FORMAT: 1A
HOST: http://api-cubbyhole.rhcloud.com

# Cubbyhole
Cubbyhole API is the webservice for accessing all resources of the Cubbyhole application.

## Authentication
The cubbyhole API uses OAuth 2.0 authentication method with 3 types of grant :

+ **password** : for all proprietary applications, token can be requested with only the user credentials.
+ **refresh_token** : the refresh token which has a bigger lifetime than the access token, can be used to regenerate access token and refresh token.
+ **authorization_code** : authorization flow must be used to request an authorization code, and the authorization code can be traded to an access token.

The token request must be performed on :
`POST: http://api-cubbyhole.rhcloud.com/oauth/token`

See details below in the Authentication section.


The authorization flow starts by performing :
`GET: http://api-cubbyhole.rhcloud.com/oauth/authorise`

The user will be prompted to login if he's not and the authorisation dialog box will appear. If the user accept, he will be redirected to the redirect_uri speciffied at the application registration.

## Media Types
Where applicable this API uses the [JSON](http://json.org/) media-type to represent resources states and affordances.

Requests with a message-body are using plain JSON to set or update resource states.

## Error States
The common [HTTP Response Status Codes](https://github.com/for-GET/know-your-http-well/blob/master/status-codes.md) are used. If an error occurs while using the API, the error output will be like this :

```json
{
    "status": 400
    "errors": [
        {
            "code": "INVALID_FIELD",
            "message": "Invalid property",
            "field": "password",
            "rule": "required|min:6|max:30",
        }
    ]
    "response": null
}
```

# Group Users
User related resources of the **Cubbyhole API**. Users are part of the authentication process to the API.

## User [/users/{id}{?access_token&_plan&date&email&password}]
A single User object with all its details

+ Parameters
    + id (required, string) ... Alphanumeric `id` of the User to perform action with.
    + access_token (required, string) ... OAuth 2.0 access token to allow access to the API.

+ Model (application/json)
    + Body
    
            {
                "status":200,
                "errors":null,
                "response": {
                    "email": "test@cubbyhole.com",
                    "date": "2014-06-13T15:19:04.405Z",
                    "files": [ ],
                    "_id": "539b1668526905000050d5fb",
                    "_plan": null
                }
            }
            

### Retrieve a User [GET]
+ Request (application/json)

            {
                "access_token": "9ea4b899ceb578d9ac3baeed3fbeebba1d229177"
            }

+ Response 200 (application/json)

    [User][]
    

### Edit a User [PUT]

Only email, password and plan can be edited.

+ Parameters
    + email (string, required, `example@cubbyhole.com`) ... The new email of the user.
    + password (string, required, `MyPassw0rd!`) ... The new password of the user (minimum 6 characters).
    + _plan (string, optional) ... The `id` of the new plan to apply on the user.

+ Request (application/json)
    
            { 
                "access_token": "9ea4b899ceb578d9ac3baeed3fbeebba1d229177",
                "email": "edited@cubbyhole.com"
            }

+ Response 200 (application/json)

    [User][]


### Remove a User [DELETE]
+ Request (application/json)

            {
                "access_token": "9ea4b899ceb578d9ac3baeed3fbeebba1d229177"
            }
            
+ Response 200 (application/json)

            {
                "status":200,
                "errors":null,
                "response": "User has been deleted."
            }

## Users Collection [/users{?access_token&_plan&date&email&password}]

Collection of users which contains User objects with all their details

+ Parameters
    + access_token (string, required) ... OAuth 2.0 access token to allow access to the API.
    
+ Model
    + Body
    
            {
                "status":200,
                "errors":null,
                "response": [
                    {
                        "email": "test@cubbyhole.com"
                        "date": "2014-06-13T15:19:04.405Z"
                        "files": [ ]
                        "_id": "539b1668526905000050d5fb"
                        "_plan": null
                    },
                    {
                        "email": "admin@cubbyhole.com"
                        "date": "2014-06-13T15:19:04.405Z"
                        "files": [ ]
                        "_id": "539b1668526905000050d5fb"
                        "_plan": null
                    }
                ]
            }
        

### List all Users [GET]
+ Parameters
    + date (date, optional) ... Creation date of the user
    + _plan (string, optional) ... Current plan of the user

+ Request (application/json)
        
            {
                "access_token": "9ea4b899ceb578d9ac3baeed3fbeebba1d229177",
                "_plan": null
            }

+ Response 200 (application/json)

    [Users Collection][]

### Create a User [POST]

Access token is not required to register a new user.

+ Parameters
    + access_token (optional, string) ... OAuth 2.0 access token to allow access to the API.
    + email (string, required, `example@cubbyhole.com`) ... The email of the new user.
    + password (string, required, `MyPassw0rd!`) ... The password of the new user.
    + _plan (string, optional) ... The `id` of the plan to apply to the new user. If not set, default free plan will be used.

+ Request (application/json)

        { 
            "access_token": "9ea4b899ceb578d9ac3baeed3fbeebba1d229177",
            "email": "test@cubbyhole.com",
            "password": "MyPassw0rd!"
        }

+ Response 201 (application/json)

    [User][]
    

# Group Files
Files related resources of the **Cubbyhole API**. Each file is owned by a single user that can grant access to it via the access rights or via public sharing (shortlinking). Files are contained in folders and if the folder is null, that means the file is located at the root folder of the user. Folders can't contain multiple files with the same name. File binaries are stored in the database aswell thanks to [GridFS](http://docs.mongodb.org/manual/core/gridfs/).

## File [/files/{id}{?filename&_folder}]
A single File object with all its details

+ Parameters
    + id (required, string) ... Alphanumeric `id` of the File to perform action with.

+ Model (application/json)
    + Body
    
            {
                "status":200,
                "errors":null,
                "response": {
                    "filename": "MyFile.txt",
                    "date": "2014-06-13T15:19:04.405Z",
                    "_folder": null,
                    "size": 1234567,
                    "_owner": "539b1668896906008950d5az",
                    "_id": "539b1668526905000050d5fb"
                }
            }
            

### Retrieve File informations [GET]
+ Request (application/json)

            {
                "access_token": "9ea4b899ceb578d9ac3baeed3fbeebba1d229177"
            }
            
+ Response 200 (application/json)

    [File][]


### Edit a File [PUT]
+ Parameters
    + filename (string, optional) ... The new filename that will be applied .
    + _folder (string, required) ... The `id` of the folder where the file will be moved, root folder if set to `null`.

+ Request (application/json)
    
            { 
                "access_token": "9ea4b899ceb578d9ac3baeed3fbeebba1d229177",
                "filename": "MyNewFilename.txt"
            }

+ Response 200 (application/json)

    [File][]


### Remove a File [DELETE]
+ Request (application/json)

            {
                "access_token": "9ea4b899ceb578d9ac3baeed3fbeebba1d229177"
            }

+ Response 200 (application/json)

            {
                "status":200,
                "errors":null,
                "response": "File has been deleted."
            }

## Files Collection [/files{?access_token&_folder&date}]

Collection of files which contains File objects with all their details

+ Parameters
    + access_token (string, required) ... OAuth 2.0 access token to allow access to the API.
    
+ Model
    + Body
    
            {
                "status":200,
                "errors":null,
                "response": [
                    {
                        "filename": "MyFile.txt",
                        "date": "2014-06-13T15:19:04.405Z",
                        "_folder": null,
                        "size": 1234567,
                        "_owner": "539b1668896906008950d5az",
                        "_id": "539b1668526905000050d5fb"
                    },
                    {
                        "filename": "MyFile2.txt",
                        "date": "2014-06-13T15:19:04.405Z",
                        "_folder": null,
                        "size": 1234567,
                        "_owner": "539b1668896906008950d5az",
                        "_id": "539b1668526909834050d5fb"
                    }
                ]
            }
        

### List all Files [GET]
+ Parameters
    + date (date, optional) ... Creation date of the user
    + _folder (string, optional) ... The `id` of the folder of the containing folder, root folder if `null`.

+ Request (application/json)
        
            {
                "access_token": "9ea4b899ceb578d9ac3baeed3fbeebba1d229177",
                "_plan": null
            }

+ Response 200 (application/json)

    [Files Collection][]

### Create file [POST]

File uploading isn't allowed by `POST`. File upload must be performed by websocket. See file uploading section.

+ Response 403 (application/json)
    
            {
                "status": 404,
                "errors": "Given path isn't handled",
                "response": null
            }

# Group Folders
Folders related resources of the **Cubbyhole API**. Each folder is owned by a single user, and can contain any amount of file/folder regarding to your plan limit.

## Folder [/folders/{id}{?foldername&_parent}]
A single Folder object with all its details

+ Parameters
    + id (required, string) ... Alphanumeric `id` of the Folder to perform action with. Has example value.

+ Model (application/json)
    + Body
    
            {
                "status":200,
                "errors":null,
                "response": {
                    "foldername": "MyFolder",
                    "date": "2014-06-13T15:19:04.405Z",
                    "_parent": null,
                    "_owner": "539b1668896906008950d5az",
                    "_id": "539b1668526905000050d5fb"
                }
            }
            

### Retrieve a Folder [GET]
+ Request (application/json)

            {
                "access_token": "9ea4b899ceb578d9ac3baeed3fbeebba1d229177"
            }
            
+ Response 200 (application/json)

    [Folder][]


### Edit a Folder [PUT]
+ Parameters
    + foldername (string, optional) ... The new folder name that will be applied .
    + _parent (string, optional) ... The id of the parent folder where the folder will be contained, root folder if set to null.

+ Request (application/json)
    
            { 
                "access_token": "9ea4b899ceb578d9ac3baeed3fbeebba1d229177",
                "filename": "MyNewFoldername.txt"
            }

+ Response 200 (application/json)

    [Folder][]


### Remove a Folder [DELETE]
+ Request (application/json)

            {
                "access_token": "9ea4b899ceb578d9ac3baeed3fbeebba1d229177"
            }

+ Response 200 (application/json)

            {
                "status":200,
                "errors":null,
                "response": "Folder has been deleted."
            }

## Folders Collection [/folders{?access_token&_parent&date&_owner&foldername}]

Collection of folders which contains Folder objects with all their details

+ Parameters
    + access_token (string, required) ... OAuth 2.0 access token to allow access to the API.
    
+ Model
    + Body
    
            {
                "status":200,
                "errors":null,
                "response": [
                    {
                        "foldername": "MyFolder",
                        "date": "2014-06-13T15:19:04.405Z",
                        "_parent": null,
                        "_owner": "539b1668896906008950d5az",
                        "_id": "539b1668526905000050d5fb"
                    },
                    {
                        "foldername": "MyFolder2",
                        "date": "2014-06-13T15:19:04.405Z",
                        "_parent": null,
                        "_owner": "539b1668896906008950d5az",
                        "_id": "539b166852789879797975fb"
                    }
                ]
            }
        

### List all Folders [GET]
+ Parameters
    + date (date, optional) ... Creation date of the user
    + _parent (string, optional) ... The `id` of the parent folder where the folder is contained. If `null`, root folder.

+ Request (application/json)
        
            {
                "access_token": "9ea4b899ceb578d9ac3baeed3fbeebba1d229177",
                "_parent": null
            }

+ Response 200 (application/json)

    [Folders Collection][]

### Create Folder [POST]
+ Parameters
    + foldername (string, required, `MyFolder`) ... The name of the new folder.
    + _parent = `null`(string, optional) ... The `id` of the parent folder in which the new folder will be contained. Parent will be root if null.
    + _owner (string, optional) ... The `id` of the owner of the folder.

+ Request (application/json)

        { 
            "access_token": "9ea4b899ceb578d9ac3baeed3fbeebba1d229177",
            "foldername": "MyFolder",
            "_owner": "539b1668896906008950d5az"
        }

+ Response 201 (application/json)

    [Folder][]

## File Hierarchy [/hierarchy{?access_token}]

### Retrieve the account hierarchy [GET]
+ Parameters
    + access_token (required, string) ... OAuth 2.0 access token to allow access to the API.
    
+ Request (application/json)

            {
                "access_token": "9ea4b899ceb578d9ac3baeed3fbeebba1d229177"
            }
    
+ Response 200 (application/json)

            {
                "status":200,
                "errors":null,
                "response": {
                    "folders": [
                        {
                            "foldername": "MyFolder",
                            "_owner": "539b1668896906008950d5az",
                            "_id": "539b1668526909834050d5fb"
                            "files": []
                        }
                    ],
                    "files": [
                        {
                            "filename": "MyFile2.txt",
                            "date": "2014-06-13T15:19:04.405Z",
                            "_folder": null,
                            "size": 1234567,
                            "_owner": "539b1668896906008950d5az",
                            "_id": "539b1668526909834050d5fb"
                        }
                    ]
                }
            }

# Group AccessRights
AccessRight related resources of the **Cubbyhole API**. Access rights are used to grant read or read/write access to another user that is not the owner of the file. Only the owner of the file can grant such access.

## AccessRight [/rights/{id}{?access_token&access}]
A single AccessRight object with all its details

+ Parameters
    + id (required, string) ... Alphanumeric `id` of the AccessRight to perform action with.
    + access_token (required, string) ... OAuth 2.0 access token to allow access to the API.

+ Model (application/json)
    + Body
    
            {
                "status":200,
                "errors":null,
                "response": {
                    "_file": "539b1668526905000050d5fb",
                    "_user": "539b1668526839545750d5az",
                    "_id": "539b166857898790050d5bf",
                    "access": "r"
                }
            }
            

### Retrieve an AccessRight [GET]
+ Request (application/json)

            {
                "access_token": "9ea4b899ceb578d9ac3baeed3fbeebba1d229177"
            }

+ Response 200 (application/json)

    [User][]
    

### Edit an AccessRight [PUT]
+ Parameters
    + access = `r` (string, optional, `r|w`) ... The right granted to the user. `r` for read only access and `w` for read and write access.

+ Request (application/json)
    
            { 
                "access_token": "9ea4b899ceb578d9ac3baeed3fbeebba1d229177",
                "email": "edited@cubbyhole.com"
            }

+ Response 200 (application/json)

    [AccessRight][]


### Remove an AccessRight [DELETE]
+ Request (application/json)

            {
                "access_token": "9ea4b899ceb578d9ac3baeed3fbeebba1d229177"
            }
            
+ Response 200 (application/json)

            {
                "status":200,
                "errors":null,
                "response": "AccessRight has been deleted."
            }

## AccessRights Collection [/rights{?access_token&_file&_user&access}]

Collection of access rights which contains AccessRight objects with all their details

+ Parameters
    + access_token (string, required) ... OAuth 2.0 access token to allow access to the API.
    
+ Model
    + Body
    
            {
                "status":200,
                "errors":null,
                "response": [
                    {
                        "_file": "539b1668526905000050d5fb",
                        "_user": "539b1668526839545750d5az",
                        "_id": "539b166857898790050d5bf",
                        "access": "r"
                    },
                    {
                        "_file": "539b1668526905673670d5fb",
                        "_user": "539b1668526839545750d5az",
                        "_id": "539b166857898778050d5bf",
                        "access": "w"
                    }
                ]
            }
        

### List all AccessRights [GET]
+ Parameters
    + _file (string, optional) ... The `id` of the file that the access right is refering to.
    + _user (string, optional) ... The `id` of the user who will gain the access right.

+ Request (application/json)
        
            {
                "access_token": "9ea4b899ceb578d9ac3baeed3fbeebba1d229177",
                "_file": "539b1668526905000050d5fb"
            }

+ Response 200 (application/json)

    [AccessRights Collection][]

### Create an AccessRight [POST]
+ Parameters
    + _file (string, required) ... The `id` of the file that the access right is refering to.
    + _user (string, required) ... The `id` of the user who will gain the access right.
    + access = `r` (string, optional, `r|w`) ... User that the access right is refering to.

+ Request (application/json)

        { 
            "access_token": "9ea4b899ceb578d9ac3baeed3fbeebba1d229177",
            "_file": "539b1668526905673670d5fb",
            "_user": "539b1668526839545750d5az",
            "access": "w"
        }

+ Response 201 (application/json)

    [AccessRight][]

# Group Shortlink

Shortlinks can be created over any file by it's owner to allow anyone who has the link to download the file. Only one shortlink can be generated for a file.

## Shortlink [/shortlink{?access_token&file_id}]

### Create a shortlink [POST]
+ Parameters
    + access_token (string, required) ... OAuth 2.0 access token to allow access to the API.
    + file_id (string, required) ... The `id` of the file on which the shortlink will point.

+ Request (application/json)

            {
                "file_id": "539b1668526839545750d5az"
            }

+ Response 200 (application/json)
    
            {
                "status":200,
                "errors":null,
                "response": {
                    "link": "http://api-cubbyhole.rhcloud.com/f/0af4",
                    "_id": "539b1668526905000050d5fb"
                }
            }

## Downloading [/f/{shortlink}]

### Download file by shortlink [GET]
+ Parameters
    + shortlink (required, string) ... The shortlink which corresponds to the file you want to download.
    
+ Response 200

            File binaries.
            
            
            
# Group Authentication

## Access Token [/oauth/token{?grant_type&username&password&refresh_token&code}]

There's 3 ways to obtain an access_token :

+ password grant : The `grant_type` parameter will be equal to *password* and you'll need to provide `username` and `password`. This is totaly unsafe to let other applications than the proprietary use it, so it is restricted to the proprietary web client, admin pannel and mobile client.
+ refresh token : You will have to set the `grant_type` to *refresh_token* and provide the a valid refresh token with the `refresh_token` parameter.
+ authorisation : This last one will be used for the first connection on the API by an external application. The user will have to log in onto the proprietary platforme and accept to share informations with the external app to generate an authorisation code. The code will be valid for arround 30 seconds so the external app will have to request the access token and refresh token right before receiving the authorisation code. You'll just have to set the `code` with the given code and the `grant_type`to *authorisation_code*.

You will have to provide the client credentials in the Authorisation as Basic authentication header or by providing the `client_id` and `client_secret` parameters in the `POST` data. It's recomended to used the encrypted header. The Content-Type **MUST** be `application/x-www-form-urlencoded`.

### Request an access [POST]
+ Parameters
    + grant_type (required, string) ... The grant type to request the access token. Values are : *password*,*refresh_token*,*authorisation_code*
    + username (required, string) ... Email of the user to log in with.
    + password (required, string) ... The password of the user to log in with.
    + refresh_token (required, string) ... The refresh token to use to regenerate access token and refresh token.
    + code (required, string) ... The authorisation code given from authorisation flow.
    
+ Request (application/x-www-form-urlencoded)
    + Header
    
            Authorisation: Basic ODM5MjM3OTQ0ODY2MDo5MzA1ZDYyMWYxOWI4OGM2MjllZmU4NTQxMGQxZWIwYg

    + Body
    
            grant_type=password&username=test@cubbyhole.com&password=MyPassw0rd!
    
+ Response 200 (application/json)
            
            {
                "token_type": "bearer",
                "access_token": "0e988f413d6593d2adc28c1aa7cc88a9b9a41e89",
                "expires_in": 3600,
                "refresh_token": "f380cc828734979dc1612f1ff1eeb91599f940bc"
            }

## Authorisation flow [/oauth/authorise{?client_id&request_uri}]

The authorisation flow has 2 steps. First the user has to log in to the API then he'll have to authorise the foreign application to access his data. If the authorisation was given the API will return an authorisation code that can be traded for an access_token and a refresh_token. The API will automaticly redirect the user to the foreign application with the specified redirect URI and the authorisation code.

### Run authorisation flow [GET]
+ Parameters
    + client_id (required, string) ... The `id` of the application that requests access to user's data.
    + request_uri (required, string) ... The URI to redirect the user to after the authorisation flow.

+ Response 200 (text/html)

            Login page or authorise page if the user is already logged in.
